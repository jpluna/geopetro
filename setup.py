import setuptools
from distutils.core import setup


setup( 
        name='geoPetro', 
        version='0.0.9.dev2', 
        description='tools for pysit', 
        author='Juan Pablo Luna', 
        author_email='jpluna@yandex.com', 
        license='MIT',
        packages=['geoPetro',],
        install_requires=[ 'numpy', 'pyot', 'matplotlib', 'scipy>=1.11.0', 'devito', 'obspy'],
        # include_package_data=True,
        # package_data={'': ['data/*.csv']},
        # package_data={'': ['modelData/**/*']},
        # package_data={'': ['modelData/*','modelData/**/*']},
        package_data={'': ['modelData_csv/*', 'modelData_csv/**/*', 'modelData/*','modelData/**/*']}
        )
