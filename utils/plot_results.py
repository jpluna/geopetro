import os
import numpy as np
import matplotlib.pyplot as plt
import pathlib
import csv
import argparse
from mpl_toolkits.axes_grid1 import make_axes_locatable

def plot_image(data, vmin=None, vmax=None, colorbar=True, cmap="gray", title=None, figsize=(8,5)):
    """
    Plot image data, such as RTM images or FWI gradients.

    Parameters
    ----------
    data : ndarray
        Image data to plot.
    cmap : str
        Choice of colormap. Defaults to gray scale for images as a
        seismic convention.
    """
    fig = plt.figure(figsize=figsize)
    if vmin is None:
        vmin = np.min(data)
    if vmax is None:
        vmax = np.max(data)
    plot = plt.imshow(1e3*np.transpose(data),
                    vmin=vmin*1e3,
                    vmax=vmax*1e3,
                    cmap=cmap,
                    # aspect='auto',
                    interpolation='nearest')
    plot.axes.xaxis.set_visible(False)
    plot.axes.yaxis.set_visible(False)
    if title is not None:
        plt.title(title, fontdict={'fontsize': 14})

    # Create aligned colorbar on the right
    if colorbar:
        ax = plt.gca()
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        plt.colorbar(plot, cax=cax)
        cax.yaxis.set_tick_params(labelsize=10)
    fig.tight_layout()
    plt.show()
    return fig, plot, cax

def load_options(filename):
    opts = {}
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            key = row[0].lower()
            if key == 'vmin' or key == 'vmax' or key == 'figsize_width' or key == 'figsize_height': 
                # expect a float
                opts[key] = float(row[1])
            else:
                # expect a string
                opts[key] = row[1]
    return opts

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Plot results obtained with fwi_devito.')
    parser.add_argument('-i', '--input', required=True)

    args = parser.parse_args()

    dir = args.input
    for file in os.listdir(dir):
        if file.endswith('.csv'):
            print(f'Processing file {file}')
            f = pathlib.Path(f'{dir}/{file}')
            opts = load_options(f'{dir}/.{f.stem}.nfo')
            # breakpoint()
            v = np.loadtxt(f'{dir}/{file}', delimiter=',')
            if len(v.shape) == 1:
                n = len(v)
                fig = plt.figure()
                plt.plot(range(1, n+1), v)
                plt.xlabel(opts['xlabel'])
                plt.ylabel(opts['ylabel'])
                plt.title(opts['title'])
                plt.grid()
                fig.tight_layout()
            else:
                if 'vmin' in opts:
                    vmin = opts['vmin']
                else:
                    vmin = None
                if 'vmax' in opts:
                    vmax = opts['vmax']
                else:
                    vmax = None
                fig, plot, cax = plot_image(v, 
                                            vmin=vmin, 
                                            vmax=vmax, 
                                            cmap=opts['cmap'], 
                                            title=opts['title'], 
                                            figsize=[opts['figsize_width'], opts['figsize_height']])
            fig.savefig(f'{f.parent}/{f.stem}.png')
            plt.close()