# Std import block
###
import numpy as np
import matplotlib.pyplot as plt

from pysit import *
from pysit.gallery import horizontal_reflector

# Setup

#   Define Domain
pmlz = PML(0.1, 100, ftype='quadratic')

#   pmlz = Dirichlet()

z_config = (0.1, 0.8, pmlz, Dirichlet())
z_config = (0.1, 0.8, pmlz, pmlz)
#   z_config = (0.1, 0.8, Dirichlet(), Dirichlet())

d = RectangularDomain(z_config)

m = CartesianMesh(d, 301)

#   Generate true wave speed
C, C0, m, d = horizontal_reflector(m)

# Set up shots
Nshots = 1
shots = []

# Define source location and type
zpos = 0.2
source = PointSource(m, (zpos), RickerWavelet(25.0))

# Define set of receivers
receiver = PointReceiver(m, (zpos))
# receivers = ReceiverSet([receiver])

# Create and store the shot
shot = Shot(source, receiver)
# shot = Shot(source, receivers)
shots.append(shot)


# Define and configure the wave solver
# trange = (0.0,3.0)
trange = (0.0,2.0)

solver2 = ConstantDensityAcousticWave(m,
                                     kernel_implementation='cpp',
                                     formulation='scalar',
                                     spatial_accuracy_order=2,
                                     trange=trange)


print('Generating data...')
base_model = solver2.ModelParameters(m,{'C': C})

wavefields2 =  []
generate_seismic_data(shots, solver2, base_model, wavefields=wavefields2)

objective = TemporalLeastSquares(solver2)



###


import geoPetro

bbObj = geoPetro.myBB(shots, objective)

x0 = C0.reshape(-1,)

val = bbObj.f(x0)

val, grad = bbObj.fg(x0)
