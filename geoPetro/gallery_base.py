import os
import csv
import numpy as np
from pathlib import Path
from obspy import read
from scipy.interpolate import interp1d
from timeit import default_timer as timer

# def read_params_file(filename=None, fromGallery=None):
def read_params_file(case_name, from_gallery=True, custom_pars=None):
    '''
    Read case data from a csv
    '''
    if from_gallery is True: 
        __rootDir__ = os.path.dirname(__file__)
        print(f'__file__ = {__file__}')
        print(f'__rootDir__ = {__rootDir__}')
        filename_case = os.path.join(__rootDir__, 'modelData_csv', f'data_{case_name}.csv')
        # fixPath=True
    else:
        filename_case = case_name

    dict_data = {}
    with open(filename_case) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            key = row[0].lower()
            if len(row) == 2:
                # Line contains only two columns, so it can be a string or a float
                if row[1][0] != "'" and row[1][0] != '"':
                    # float
                    val = float(row[1])
                else: 
                    # string
                    val = row[1]
                    val = val.replace('"', '')
                    val = val.replace("'", '')
            else:
                # Line contains more than two coluns, so it is a float array
                ncols = len(row)-1
                val = np.zeros(ncols)
                for i,c in enumerate(row[1:]):
                    c = c.replace('[', '')
                    c = c.replace(']', '')
                    val[i] = float(c)
            dict_data[key] = val
    
    # Fill default values if they were not in the file
    _default_data(dict_data)

    if from_gallery is True:
        dict_data['file_vel'] = os.path.join(__rootDir__, 'modelData_csv', case_name, dict_data['file_vel'])
        dict_data['file_vel0'] = os.path.join(__rootDir__, 'modelData_csv', case_name, dict_data['file_vel0'])
    
    if custom_pars is not None:
        # User specified custom parameters
        for k,v in custom_pars.items():
            dict_data[k] = v

    return dict_data

def _default_data(dict_data):
    '''
    Data that is usually default and might not need to be changed
    '''

    if 'is_horizontal_reflector' in dict_data and dict_data['is_horizontal_reflector'] == 1:
        return dict_data

    if 'physical_origin' not in dict_data:
        dict_data['physical_origin'] =  np.array([0.0, 0.0])
    if 'physical_dimensions_units' not in dict_data:
        dict_data['physical_dimensions_units'] = ('m', 'm')
    if 'base_pixel_units' not in dict_data:
        dict_data['base_pixel_units'] = ('m', 'm')
    if 'factor' not in dict_data:
        dict_data['factor'] = 1
    if 'transpose_data' not in dict_data:
        dict_data['transpose_data'] = False
    if 'frequency' not in dict_data:
        dict_data['frequency'] = 5.0
    if 'type_data' not in dict_data:
        dict_data['type_data'] = 'float32'
    if 'space_order' not in dict_data:
        dict_data['space_order'] = 6
    if 'nbl' not in dict_data:
        dict_data['nbl'] = 10
    if 'nreceivers' not in dict_data:
        dict_data['nreceivers'] = max(101, 2*dict_data['nsources'])
    dict_data['base_pixels'] = dict_data['base_pixels'].astype(int)

def print_data(dict_data):
    print('\nModel parameters')
    for (k, v) in dict_data.items():
        print(f'{k}: {v}')
    print('\n')


def resample_data(arr, param):
    """ Resamples the specified parameter to the sizes, from the original."""

    # Resample the array, in each dimension, with nearest neighbor interpolation.
    y = arr
    for i in range(arr.ndim):
        x = param['base_physical_origin'][i] + np.arange(param['base_pixels'][i])*param['base_pixel_scale'][i]
        I = interp1d(x, y, copy=False, axis=i, kind='linear')
        # I = interp1d(x, y, copy=False, axis=i, kind='nearest')

        new_x = param['base_physical_origin'][i] + np.arange(param['pixels'][i])*param['pixel_scale'][i]

        # Bounds that go slightly past the true thing should extend from the right side
        new_x[new_x > param['base_physical_size'][i]] = param['base_physical_size'][i]

        y = I(new_x)

    return y

def read_model(filename, type_data=np.float32):
    '''
    Read a model file
    Currently there are two options:
    - a segy file (if the extension is .sgy or .segy), which uses obspy to read the file
    - a binary file (if the extension is different), which uses numpy
    '''
    extension = Path(filename).suffix
    if extension == '.sgy' or extension == '.segy':
        stream = read(filename)
        data = np.array([t.data for t in stream.traces])
    elif extension == '.csv':
        data = np.loadtxt(filename, delimiter=',', dtype=type_data)
    else:
        # if needed, we can create other cases here, such as text files, .mat, etc
        # for now, let's assume it's a binary file, so let's use numpy
        # some files have a float32 type. I'll assume this is the norm
        data = np.fromfile(filename, dtype=type_data)
    return data