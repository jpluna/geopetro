import time
import sys

import numpy as np

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from multiprocessing import cpu_count

from timeit import default_timer as timer

from pysit import *
from pysit.gallery import marmousi
from pysit.util.parallel import ParallelWrapShot

import geoPetro.funcs_generate_seismic_data as gen_seis
import geoPetro.gallery_pysit as glr
import geoPetro.otObj as otObj

def fwi_pysit(modelname, obj_name, custom_pars={}):
    tstart = timer()

    res = glr.get_models_pysit(modelname, from_gallery=True, custom_pars=custom_pars)
    dict_data = res['dict_data']

    C = res['C']
    Cmin = C.min()
    Cmax = C.max()
    C0 = res['C0']
    m = res['m']
    d = res['d']

    source_depth = dict_data['source_depth']
    freq = dict_data['frequency']

    if 'receiver_depth' in dict_data: 
        receiver_depth = dict_data['receiver_depth']
        print('setting receiver_depth from dictionary')
    else: 
        receiver_depth = source_depth 
        print('setting receiver_depth as source_depth')

    if 'nreceivers' in dict_data:
        nreceivers = int(min(dict_data['nreceivers'], m.parameters[0]['n']))
    else:
        nreceivers = 'max'

    # Set up shots
    shots = equispaced_acquisition(m,
                                   RickerWavelet(freq),
                                   sources=custom_pars['nsources'],
                                   source_depth=source_depth,
                                   receivers=nreceivers,
                                   receiver_depth=receiver_depth,
                                   )

    # Define and configure the wave solver
    trange = (0.0,3.0)

    solver = ConstantDensityAcousticWave(m,
                                         spatial_accuracy_order=6,
                                         trange=trange,
                                         kernel_implementation='cpp')

    # Generate synthetic Seismic data
    sys.stdout.write('Generating data...')
    base_model = solver.ModelParameters(m,{'C': C})
    shots, solver = gen_seis.generate_seismic_data(shots, 
                                                   solver, 
                                                   base_model, 
                                                   ncores=custom_pars['ncores'])
    sys.stdout.flush()

    # Define the objective function
    objective = obj_name(solver, ncores=custom_pars['ncores'])
    if obj_name == otObj.myOTx:
        objective.__myInit__(shots)
    elif obj_name == otObj.UOTx:
        objective.__myInit__(shots)
    elif obj_name == otObj.UOTx2:
        objective.__myInit__(shots)
    elif obj_name == otObj.UOTSx:
        objective.__myInit__(shots)
    elif obj_name == otObj.UOTSx2:
        objective.__myInit__(shots)
    else:
        objective.__myInit__()

    # Define the inversion algorithm
    invalg = LBFGS(objective, memory_length=10)

    initial_value = solver.ModelParameters(m, {'C': C0})

    # Execute inversion algorithm
    print('Running LBFGS...')
    tt = time.time()

    status_configuration = {'value_frequency'           : 1,
                            'objective_frequency'       : 1}

    result = invalg(shots, initial_value, custom_pars['niter'],
                    line_search='backtrack',
                    status_configuration=status_configuration,
                    verbose=True)

    print('Run time:  {0}s'.format(time.time()-tt))

    tend = timer()
    print(f'Total time: {tend-tstart:.2f}s')

    model = result.C.reshape(m.shape(as_grid=True))

    vals = list()
    for k,v in list(invalg.objective_history.items()):
        vals.append(v)
    obj_vals = np.array(vals)

    from scipy.io import savemat

    out = {'result': model,
            'true': C.reshape(m.shape(as_grid=True)),
            'initial': C0.reshape(m.shape(as_grid=True)),
            'obj': obj_vals
            }
    savemat('model_recon.mat',out)

    def plot_image(data, vmin=None, vmax=None, colorbar=True, cmap="gray", title=None, figsize=(8,5)):
        """
        Plot image data, such as RTM images or FWI gradients.

        Parameters
        ----------
        data : ndarray
            Image data to plot.
        cmap : str
            Choice of colormap. Defaults to gray scale for images as a
            seismic convention.
        """
        fig = plt.figure(figsize=figsize)
        if vmin is None:
            vmin = np.min(data)
        if vmax is None:
            vmax = np.max(data)
        plot = plt.imshow(np.transpose(data),
                        vmin=vmin,
                        vmax=vmax,
                        cmap=cmap,
                        aspect='auto',
                        interpolation='nearest')
        plot.axes.xaxis.set_visible(False)
        plot.axes.yaxis.set_visible(False)
        if title is not None:
            plt.title(title, fontdict={'fontsize': 14})

        # Create aligned colorbar on the right
        if colorbar:
            ax = plt.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size="5%", pad=0.05)
            plt.colorbar(plot, cax=cax)
            cax.yaxis.set_tick_params(labelsize=10)
        # fig.tight_layout()
        plt.show()
        return fig, plot, cax

    plt.ion()

    vmin = 1.0*Cmin
    vmax = 1.0*Cmax


    fig, plot, cax = plot_image(out['result'], vmin=vmin, vmax=vmax, cmap='viridis', title='Solution')
    fig.savefig('C.png')

    Cmat = C.reshape(m.parameters[0]['n'], m.parameters[1]['n'])
    fig, plot, cax = plot_image(Cmat, vmin=vmin, vmax=vmax, cmap='viridis', title='True model')
    fig.savefig('Ctrue.png')

    C0mat = C0.reshape(m.parameters[0]['n'], m.parameters[1]['n'])
    fig, plot, cax = plot_image(C0mat, vmin=vmin, vmax=vmax, cmap='viridis', title='Initial model')
    fig.savefig('C0.png')


    