import os
import copy
import matplotlib.pyplot as plt
import numpy as np
from timeit import default_timer as timer
from scipy import optimize

import ot
from examples.seismic import demo_model, AcquisitionGeometry, plot_velocity, plot_shotrecord, Receiver, plot_image


import gallery_devito as glr
# import geoPetro.myDevito as myDevito
# import geoPetro.gallery_devito as glr
import myDevito

plt.ion()
# plt.ioff()
plt.close('all')

k = 0 # iteration counter
# tacc = 0 # elapsed time
t0 = timer() # timer to check total time and stop solver



def sl2(d_pred, d_obs, data=None):
    '''
    both matrices (2D np arrays) are the sismograms. Each colum is a trace.
    the function returns the misfit and adjoint source
    '''
    residual =  d_pred - d_obs
    lres = residual.reshape(-1)
    fval = 0.5 * lres.dot(lres)
    return fval, residual

class KL():
    def __init__(self, tau=1e-4, kernel='b'):
        self.tau = tau
        self.kernel = kernel

    def smooth(self, x):
        if self.kernel == 'a':
            aux =  1 + np.exp(-x / self.tau)
            return x + self.tau * np.log(aux), 1.0/ aux
        elif self.kernel == 'b':
            aux = np.sqrt( x * x + 4 * self.tau * self.tau)
            return (x + aux) / 2, (1 + (x / aux)) / 2
        else:
            raise ValueError('Invalid kernel')
    
    def vkl(self, a, b):
        '''
        computes the misfit of a into b
        '''
        zero = 1e-8
        pb = b>0
        pa = a>zero
        val = 0
        if any(pb):
            if any(pa):
                indAux = pa & pb
                val+= (a[indAux] * np.log(a[indAux]/b[indAux]) -a[indAux] + b[indAux]).sum()
            if any(~pa):
                val += b[ (~pa) & pb].sum()
        return val
        
    def klx(self, d_pred, d_obs):
        '''
        both matrices (2D np arrays) are the sismograms. Each colum is a trace.
        the function terutns the misfit and adjoint source
        '''
        simSeis = d_pred.reshape(-1,)
        obsSeis = d_obs.reshape(-1,)

        xMass1, dxMass = self.smooth(simSeis)
        xMass2, _ = self.smooth(-obsSeis)
        xMass = xMass1 + xMass2
        del xMass1 
        del xMass2
        yMass1, dyMass = self.smooth(-simSeis)
        yMass2, _  = self.smooth(obsSeis)

        yMass = yMass1 + yMass2
        del yMass1 
        del yMass2
        xDivy = xMass/yMass

        val = self.vkl(xMass, yMass)
        indAux = (xMass>0) & (yMass>0)
        r = np.zeros_like(simSeis)
        r[indAux] = np.log(xDivy[indAux]) * dxMass[indAux] - (1 -  xDivy[indAux]) * dyMass[indAux]

        fval= val
        residual = r.reshape(d_obs.shape)

        return fval, residual

class UOT:
    # Virtual class. Derived UOT classes should use this one as base
    def __init__(self, geometry, kernel='b', tau=1e-4, transl=1e-3):
        self.kernel = kernel
        self.tau = tau
        self.geometry = geometry
        self.dt = geometry.dt
        coords = geometry.rec_positions
        self.otM = ot.dist(coords,coords,'sqeuclidean')
        self.transl = transl

    def smooth(self, x):
        if self.kernel == 'a':
            aux =  1 + np.exp(-x / self.tau)
            return x + self.tau * np.log(aux), 1.0/ aux
        elif self.kernel == 'b':
            aux = np.sqrt( x * x + 4 * self.tau * self.tau)
            return (x + aux) / 2, (1 + (x / aux)) / 2
        else:
            raise ValueError('Invalid kernel')
    
    def defineRm(self, a, b):
        am = a.sum()
        bm = b.sum()
        aux = am/bm if am/bm >1 else bm/am
        if aux < 1.01:
            rm = 100
        elif aux < 1.5:
            rm = 10
        elif aux < 2:
            rm = 5
        else:
            rm = 1e-3

        return rm

class UOTx(UOT):      
    def uot(self, simSeis, obsSeis):
        # simSeis and obsSeis are devito Data types, which are numpy arrays with some other things
        # pot gives weird errors if we keep them that way, so first we need to 
        # extract only the array data
        simSeis = np.array(simSeis)
        obsSeis = np.array(obsSeis)

        M = self.otM

        factor = M.max() 
        M = M/factor
        # reg=2.e+0/factor
        reg=1.0
        # regm=1.e-8


        xMass1, dxMass = self.smooth(simSeis)
        xMass2, _ = self.smooth(-obsSeis)
        xMass = xMass1 + xMass2
        del xMass1 
        del xMass2
        yMass1, dyMass = self.smooth(-simSeis)
        yMass2, _  = self.smooth(obsSeis)

        yMass = yMass1 + yMass2
        del yMass1 
        del yMass2

        val = 0
        adjS = np.empty_like(simSeis)

        transl_type = 'threshold'

        for i, (xm, ym) in enumerate(zip(xMass, yMass)): 
            regm = self.defineRm(xm, ym)

            if transl_type == 'only_x':
                xm += self.transl
            elif transl_type == 'only_y':
                ym += self.transl
            elif transl_type == 'both':
                xm += self.transl
                ym += self.transl
            elif transl_type == 'threshold':
                # Only translate if xm or ym is below the threshold
                thresh = 1e-9
                xm[xm < thresh] += self.transl
                ym[ym < thresh] += self.transl

            gamma, dic = ot.unbalanced.sinkhorn_stabilized_unbalanced(xm, ym, M, reg, regm, log=True) 
            gamma1 = gamma.sum(axis=1)
            gammaT1 = gamma.sum(axis=0)
            val += (gamma * M).sum() +  reg * (gamma1 * (np.log(gamma1/ xm)-1) + xm).sum() + reg * (gammaT1 * (np.log(gammaT1/ym)-1) + ym).sum()
            adjS[i,:] = reg * ( (1 -  gamma.sum(axis=1)/(xm)) * dxMass[i] - (1 -  gamma.sum(axis=0)/(ym)) * dyMass[i]  )

            if np.isnan(val) or np.any(np.isnan(adjS)):
                breakpoint()
        return val * self.dt, adjS

class UOTx2(UOT):
    def uot(self, simSeis, obsSeis):
        '''
        the result should be scaled with dt
        ''' 
        # simSeis and obsSeis are devito Data types, which are numpy arrays with some other things
        # pot gives weird errors if we keep them that way, so first we need to 
        # extract only the array data
        simSeis = np.array(simSeis)
        obsSeis = np.array(obsSeis)

        # computing OT distance
        M = self.otM

        factor = M.max() 
        M = M/factor
        # reg=2.e+0/factor
        reg=1.0
        # regm=1.e-8

        xMassPv, dxMassPv = self.smooth(simSeis)
        xMassP0, _ = self.smooth(obsSeis)

        mxMassPv, mdxMassPv = self.smooth(-simSeis)
        mxMassP0, _ = self.smooth(-obsSeis)

        transl_type = 'threshold'

        val = 0
        adjS = np.zeros_like(simSeis)
        # for pv, p0 in [(xMassPv, xMassP0), (mxMassPv, mxMassP0)]:
        for k , (pv, p0) in enumerate([(xMassPv, xMassP0), (mxMassPv, mxMassP0)]):
            for i, (xm, ym) in enumerate(zip(pv, p0)):
                if transl_type == 'only_x':
                    xm += self.transl
                elif transl_type == 'only_y':
                    ym += self.transl
                elif transl_type == 'both':
                    xm += self.transl
                    ym += self.transl
                elif transl_type == 'threshold':
                    thresh = 1e-9
                    xm[xm < thresh] += self.transl
                    ym[ym < thresh] += self.transl

                regm = self.defineRm(xm, ym)
                gamma, dic = ot.unbalanced.sinkhorn_stabilized_unbalanced(xm, ym, M, reg, regm, log=True) 
                gamma1 = gamma.sum(axis=1)
                gammaT1 = gamma.sum(axis=0)
                val += (gamma * M).sum() +  reg * (gamma1 * (np.log(gamma1/ xm)-1) + xm).sum() + reg * (gammaT1 * (np.log(gammaT1/ym)-1) + ym).sum()
                adjS[i,:] += ( -1 if k>0 else 1) * reg * ( (1 -  gamma.sum(axis=1)/(xm)) * (mdxMassPv[i]  if k>0 else dxMassPv[i]) ) 

                if np.isnan(val) or np.any(np.isnan(adjS[i,:])):
                    breakpoint()

        # print('adjSource')
        # breakpoint()
        return val * self.dt, adjS

# class UOTs(UOT):
#     def optTransportCost(self, gamma, xm, ym, M, reg, regm):
#         gamma1 = gamma.sum(axis=1)
#         gammaT1 = gamma.sum(axis=0)
#         val = (gamma * M).sum() +  reg * (gamma1 * (np.log(gamma1/ xm)-1) + xm).sum() + reg * (gammaT1 * (np.log(gammaT1/ym)-1) + ym).sum()
#         return val
    
#     def uots(self, x, y, M, reg):
#         vals = []
#         rmAx1 = np.zeros_like(x) # Ax \times ones
#         regm = self.defineRm(x, y)
#         for i, (xm, ym) in enumerate((x,y), (x,x), (y,y)):
#             gamma, dic = ot.unbalanced.sinkhorn_stabilized_unbalanced(xm, ym, M, reg, regm, log=True) 
#             vals.append( self.optTransportCost(gamma,xm, ym, M, reg, regm))
#             if i == 0:
#                 rmAx1 -= gamma.sum(axis=1)
#             elif i == 1:
#                 rmAx1 += 0.5 *(gamma.sum(axis=0) + gamma.sum(axis=1)) 
#         rmAx1 = regm * (rmAx1/x)
#         val = vals[0] - 0.5 * sum(vals[1:]) + (reg*0.5* ((x.sum() - y.sum())**2))
#         return val, rmAx1
    
#     def uot(self, simSeis, obsSeis):
#         '''
#         the result should be scaled with dt
#         ''' 
#         # computing OT distance
#         M = self.otM

#         factor = M.max() 
#         M = M/factor
#         # reg=2.e+0/factor
#         reg=1.0
#         # regm=1.e-8

#         xMass1, dxMass = self.smooth(simSeis)
#         xMass2, _ = self.smooth(-obsSeis)
#         xMass = xMass1 + xMass2
#         del xMass1 
#         del xMass2
#         yMass1, dyMass = self.smooth(-simSeis,kernel=self.kernel)
#         yMass2, _  = self.smooth(obsSeis, kernel=self.kernel)

#         yMass = yMass1 + yMass2
#         del yMass1 
#         del yMass2

#         val = 0

#         for i, (xm, ym) in enumerate(zip(xMass, yMass)): 
#             val += self.uotS(xm, ym, M, reg)[0]

#         return val * self.dt

# class UOTs2(UOT):
#     def optTransportCost(self, gamma, xm, ym, M, reg, regm):
#         gamma1 = gamma.sum(axis=1)
#         gammaT1 = gamma.sum(axis=0)
#         val = (gamma * M).sum() +  reg * (gamma1 * (np.log(gamma1/ xm)-1) + xm).sum() + reg * (gammaT1 * (np.log(gammaT1/ym)-1) + ym).sum()
#         breakpoint()
#         return val
    
#     def uots(self, x, y, M, reg):
#         vals = []
#         rmAx1 = np.zeros_like(x) # Ax \times ones
#         regm = self.defineRm(x, y)
#         for i, (xm, ym) in enumerate([(x,y), (x,x), (y,y)]):
#             gamma, dic = ot.unbalanced.sinkhorn_stabilized_unbalanced(xm, ym, M, reg, regm, log=True) 
#             vals.append( self.optTransportCost(gamma,xm, ym, M, reg, regm))
#             if i == 0:
#                 rmAx1 -= gamma.sum(axis=1)
#             elif i == 1:
#                 rmAx1 += 0.5 *(gamma.sum(axis=0) + gamma.sum(axis=1)) 
#         rmAx1 = regm * (rmAx1/x)
#         val = vals[0] - 0.5 * sum(vals[1:]) + (reg*0.5* ((x.sum() - y.sum())**2))

#         if np.isnan(val):
#             breakpoint()

#         return val, rmAx1

#     def uot(self, simSeis, obsSeis):
#         '''
#         the result should be scaled with dt
#         ''' 
#         # computing OT distance

#         simSeis = np.array(simSeis)
#         obsSeis = np.array(obsSeis)

#         M = self.otM

#         factor = M.max() 
#         M = M/factor
#         reg=1 
#         regm=1.e-8

#         xMassPv, dxMassPv =  self.smooth(simSeis)
#         xMassP0 = (np.abs(obsSeis) + obsSeis)/2
#         mxMassP0 = xMassP0 - obsSeis

#         mxMassPv = xMassPv -simSeis 

#         breakpoint()

#         val = 0
#         adjS = np.zeros_like(simSeis)
#         for k , (pv, p0, dv) in enumerate([(xMassPv, xMassP0, dxMassPv), (mxMassPv, mxMassP0, dxMassPv-1)]):
#             for i, (xm, ym,dx) in enumerate(zip(pv, p0, dv)): 
#                 valAux, rmAx1 = self.uots(xm,ym, M, reg)
#                 val += valAux
#                 adjS[i,:] +=  (rmAx1 + reg*(xm.sum() - ym.sum())) * dx

#         # print('adjSource')
#         return val * self.dt, adjS







class fwi:
    def __init__(self, modelname, custom_pars=None, cmap='viridis'):
        if modelname.split('.')[-1] == 'csv':
            # Model is not in the gallery
            self.from_gallery = False
        else:
            # Model is in the gallery
            self.from_gallery = True
        self.modelname = modelname

        # Load parameters
        self.pars = glr.read_params_file(modelname, self.from_gallery, custom_pars)

        # breakpoint()

        # Set parameters that were not defined
        self._set_default_pars()

        # breakpoint()

        # Check if the output dir exists
        if not os.path.exists(self.pars['outputdir']):
            os.makedirs(self.pars['outputdir'])

        self.iter = {}
        self.iter['k'] = []
        self.iter['f'] = []
        self.iter['error'] = []

        self.misfit_fn = None
    
    def _set_default_pars(self):
        if 'nsources' not in self.pars:
            self.pars['nsources'] = 1
        if 'ncores' not in self.pars:
            self.pars['ncores'] = 1
        if 'niter' not in self.pars:
            self.pars['niter'] = 10
        if 'cmap' not in self.pars:
            self.pars['cmap'] = 'viridis'
        if 'outputdir' not in self.pars:
            self.pars['outputdir'] = '.'
        if 'method_optim' not in self.pars:
            self.pars['method_optim'] = 'L-BFGS-B'
        if 'maxtime' not in self.pars:
            self.pars['maxtime'] = 1e9

    def set_misfit_function(self, misfit):
        # Set the misfit function
        if isinstance(misfit, str):
            if misfit == 'l2':
                # Misfit function: L2
                misfit_fn = sl2
            elif misfit == 'kl':
                # Misfit function: KL
                kl = KL(tau=1e-4, kernel='b')
                misfit_fn = kl.klx
            elif misfit == 'uotx':
                # Misfit function: UOTx
                uotx = UOTx(self.geometry1)
                misfit_fn = uotx.uot
            elif misfit == 'uotx2':
                # Misfit function: UOTx2
                uotx2 = UOTx2(self.geometry1)
                misfit_fn = uotx2.uot
            else:
                print('Invalid misfit function. Using L2')
                misfit_fn = sl2
        else:
            # misfit is a function
            misfit_fn = misfit
        self.misfit_fn = misfit_fn

    def get_models(self):
        self.model1, self.model0, _ = glr.get_models_devito(self.modelname, 
                                                            from_gallery=self.from_gallery, 
                                                            custom_pars=self.pars)
        self.pars['vmin'] = self.model1.vp.data.min() * 0.95
        self.pars['vmax'] = self.model1.vp.data.max() * 1.05

        v0 = self.model0.vp.data[self.model0.nbl:-self.model0.nbl, self.model0.nbl:-self.model0.nbl]
        self.v0 = copy.deepcopy(v0) # a hack just so we can plot the difference later

        # Set figures proportions
        shape = np.array(self.model1.vp.data.shape)
        prop = shape/shape.max()
        self.pars['figsize'] = np.array([8, 8]) * prop

    def get_geometry(self):
        self.geometry1, self.geometry0 = glr.get_geometry_devito(self.pars, 
                                                                 self.model1, 
                                                                 self.model0)


    def plot_model_initial_true(self):
        # Plot initial and true velocities
        fig, plot, cax = myDevito.plot_image(self.model0.vp.data[self.model0.nbl:-self.model0.nbl, self.model0.nbl:-self.model0.nbl], 
                                            vmin=self.pars['vmin'], 
                                            vmax=self.pars['vmax'], 
                                            cmap=self.pars['cmap'], 
                                            title=f'Initial model - {self.modelname}', 
                                            figsize=self.pars['figsize'])
        filename = f'initial_model_{self.modelname}'
        fig.savefig(f'{self.pars["outputdir"]}/{filename}.png')
        plt.close()
        
        fig, plot, cax = myDevito.plot_image(self.model1.vp.data[self.model1.nbl:-self.model1.nbl, self.model1.nbl:-self.model1.nbl], 
                            vmin=self.pars['vmin'], 
                            vmax=self.pars['vmax'], 
                            cmap=self.pars['cmap'], 
                            title=f'True model - {self.modelname}', 
                            figsize=self.pars['figsize'])
        filename = f'true_model_{self.modelname}'
        fig.savefig(f'{self.pars["outputdir"]}/{filename}.png')
        plt.close()
    
    def plot_true_model_sources_receivers(self):
        plt.figure()
        plot_velocity(self.model1, source=self.geometry1.src_positions, receiver=self.geometry1.rec_positions[::4, :])
        plt.suptitle('Model with sources and receivers')
        plt.savefig(f'{self.pars["outputdir"]}/model_sources_receivers.png')
    
    def plot_model_error(self):
        # Plot model error
        fig = plt.figure(figsize=self.pars['figsize'])
        plt.plot(self.iter['k'], self.iter['error'])
        plt.title('Model error')
        plt.xlabel('Iteration number')
        plt.ylabel('L2-model error')
        plt.grid()
        fig.tight_layout()
        plt.savefig(f'{self.pars["outputdir"]}/iter.png')
        np.savetxt(f'{self.pars["outputdir"]}/iter.csv', self.iter['error'], fmt='%.4f', delimiter=',')

    def plot_misfit(self):
        fig = plt.figure(figsize=self.pars['figsize'])
        plt.semilogy(self.iter['k'], self.iter['f'])
        plt.title('Misfit function')
        plt.xlabel('Iteration number')
        plt.ylabel('Misfit')
        plt.grid()
        fig.tight_layout()
        plt.savefig(f'{self.pars["outputdir"]}/misfit.png')
        np.savetxt(f'{self.pars["outputdir"]}/misfit.csv', self.iter['f'], fmt='%.4f', delimiter=',')
        
    def plot_results(self):
        # Plot results        
        fig, plot, cax = myDevito.plot_image(self.model0.vp.data[self.model0.nbl:-self.model0.nbl, self.model0.nbl:-self.model0.nbl], 
                                            vmin=self.pars['vmin'], 
                                            vmax=self.pars['vmax'], 
                                            cmap=self.pars['cmap'], 
                                            title='Solution', 
                                            figsize=self.pars['figsize'])
        filename = 'res'
        fig.savefig(f'{self.pars["outputdir"]}/{filename}.png')
        plt.close()
        np.savetxt(f'{self.pars["outputdir"]}/{filename}.csv', 
                   self.model0.vp.data[self.model0.nbl:-self.model0.nbl, self.model0.nbl:-self.model0.nbl], 
                   fmt='%.4f', 
                   delimiter=',')
        
        dif = self.model0.vp.data[self.model0.nbl:-self.model0.nbl, self.model0.nbl:-self.model0.nbl] - self.v0
        fig, plot, cax = myDevito.plot_image(dif, 
                                            cmap=self.pars['cmap'], 
                                            title='Difference', 
                                            figsize=self.pars['figsize'])
        filename = 'dif'
        fig.savefig(f'{self.pars["outputdir"]}/{filename}.png')
        plt.close()
        np.savetxt(f'{self.pars["outputdir"]}/{filename}.csv', 
                   dif, 
                   fmt='%.4f', 
                   delimiter=',')

    def optimize_single_freq(self):
        if self.misfit_fn is None:
            raise ValueError('Misfit function was not set')

        model1 = self.model1
        model0 = self.model0
        geometry1 = self.geometry1
        geometry0 = self.geometry0
        ###

        print(f'\n\nModel size: {model1.vp.data.shape}\n\n')

        def fwi_callback(intermediate_result):
            xk = intermediate_result['x']
            f = intermediate_result['fun']

            vp = model1.vp.data[model1.nbl:-model1.nbl, model1.nbl:-model1.nbl]

            vp_vec = vp.reshape(-1).astype(np.float64)
            model_error_curr = np.linalg.norm((xk-vp_vec)/vp_vec)
            
            global k
            k += 1
            print(f'model error iter {k} = {model_error_curr}')
            print(f'fun = {f}')

            self.iter['k'].append(k)
            self.iter['f'].append(f)
            self.iter['error'].append(model_error_curr)

            myDevito.plot_image(model0.vp.data[model0.nbl:-model0.nbl, model0.nbl:-model0.nbl], 
                                vmin=self.pars['vmin'], 
                                vmax=self.pars['vmax'], 
                                cmap=self.pars['cmap'], 
                                title=f'Iteration {k}, f = {1e3 * geometry0.f0} Hz', 
                                figsize=self.pars['figsize'])
            filename = f'res_iter{k}'
            plt.savefig(f'{self.pars["outputdir"]}/{filename}.png')
            plt.close()

            np.savetxt(f'{self.pars["outputdir"]}/{filename}.csv', 
                    model0.vp.data[model0.nbl:-model0.nbl, model0.nbl:-model0.nbl],
                    fmt='%.4f', 
                    delimiter=',')
            
            global t0
            t1 = timer()
            ttotal = t1-t0
            print(f'Total elapsed time: {t1-t0}s')
            if ttotal >= self.pars['maxtime']:
                raise StopIteration('Total time exceeded')


            
        # Initial guess
        v0 = model0.vp.data[model0.nbl:-model0.nbl, model0.nbl:-model0.nbl]
        x0 = v0.reshape(-1).astype(np.float64)
        
        bb = myDevito.myBB(model1,
                           geometry1,
                           model0, 
                           geometry0, 
                           ncores=self.pars['ncores'], 
                           misfitFunction=self.misfit_fn)
        bb_fn = bb.fg
        ftol = 0
        tstart = timer()

        if self.pars['method_optim'] not in ['L-BFGS-B', 'CG']:
            print('Parameter "method_optim" invalid. Using L-BFGS-B')
            self.pars['method_optim'] = 'L-BFGS-B'

        result = optimize.minimize(bb_fn, 
                            x0, 
                            method=self.pars['method_optim'], 
                            jac=True, 
                            callback=fwi_callback, 
                            options={'maxiter': self.pars['niter'], 'disp': True}) 
        
        vp = result['x'].reshape(model1.shape)
        
        tend = timer()
        print(f'Total time: {tend-tstart:.2f}s')
        return vp

    def optimize_multi_freq(self, list_freqs):
        if self.misfit_fn is None:
            raise ValueError('Misfit function was not set')
        
        for f in list_freqs:
            print(f'Solving fwi for frequency {f} Hz')
            self.pars['frequency'] = f

            # Update geometry for current frequency
            self.get_geometry()

            # breakpoint()

            # Optimize for current frequency
            self.optimize_single_freq()

