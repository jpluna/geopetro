import numpy as np

def smooth(x, tau=1e-4, kernel='a'):
    if kernel == 'a':
        aux =  1 + np.exp(-x / tau)
        return x + tau * np.log(aux), 1.0/ aux
    elif kernel == 'b':
        aux = np.sqrt( x * x + 4 * tau * tau)
        return (x + aux) / 2, (1 + (x / aux)) / 2
    else:
        raise ValueError('Invalid kernel')
