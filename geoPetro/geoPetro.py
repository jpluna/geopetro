'''
This module provides an oracle class
myBB(shots, objective)
'''


import numpy as np
class myBB():
    objective=None
    shots=None
    base_model=None
    base_mode_data=None
    n=None #problem dimension

    def __init__(self, shots, objective):
        self.objective = objective
        self.shots=shots
        self.base_model = objective.solver.ModelParameters(objective.solver.mesh)
        self.base_model_data = self.base_model.data.reshape(-1,)
        self.n = len(self.base_model_data)

    def f(self, x):
        self.base_model_data[:] = x
        val = self.objective.evaluate(self.shots, self.base_model)
        return val

    def fg(self, x):
        # base_model = self.objective.solver.ModelParameters(self.objective.solver.mesh)
        # base_model.data[:] = x.reshape(-1,1)
        self.base_model_data[:] = x

        aux_info = {'objective_value': (True, None), 
                # 'residual_norm': (True, None),
                }

        gradient = self.objective.compute_gradient(self.shots, self.base_model, aux_info=aux_info) # , **objective_arguments)
        grad = gradient.data.reshape(-1,)
        val = aux_info['objective_value'][1]
        # val = self.f(x)
        return val, -grad


    def rfg(self, x, r=0.01, h=1e-4):
        # base_model = self.objective.solver.ModelParameters(self.objective.solver.mesh)
        # base_model.data[:] = x.reshape(-1,1)
        self.base_model_data[:] = x

        val = self.f(x)


        # computing the se o findex for computing nonzeros elements in gradient
        ind = []
        while len(ind)< r * self.n:
            i = np.random.randint(self.n)
            if i not in ind: ind.append(i)

        g = np.zeros_like(x)
        aux = np.array(x)

        for k in ind: 
            aux[ind] = x[ind]
            aux[k] += h
            valAux = self.f(aux)
            g[k] = (valAux - val) / h
        return val, g

    def seismograms(self, x, shot):
        # base_model = self.objective.solver.ModelParameters(self.objective.solver.mesh)
        # base_model.data[:] = x.reshape(-1,1)
        self.base_model_data[:] = x

        aux_info = {'objective_value': (True, None), 
                # 'residual_norm': (True, None),
                }
        simSeismo, obsSeismo = self.objective._residual( shot, self.base_model, dWaveOp=[], wavefield=None)
        return simSeismo, obsSeismo

    def adj(self, x, shot):
        # base_model = self.objective.solver.ModelParameters(self.objective.solver.mesh)
        # base_model.data[:] = x.reshape(-1,1)
        self.base_model_data[:] = x

        auxVal, g, r = self.objective._gradient_helper(shot, self.base_model, ignore_minus=True) #, **kwargs)
        return r
