# This file has functions that were adapted from pysit/modeling/data_modeling.py
# to allow for parallel processing using the multiprocessing module
# 
# Author: Marcelo Cordova
# 2023/05

import numpy as np
from multiprocessing import Pool, cpu_count
from timeit import default_timer as timer


def _generate_shot_data_time_shot(dict_shot):
    tstart = timer()
    ind = dict_shot['ind']
    shot = dict_shot['shot']
    solver = dict_shot['solver']
    model = dict_shot['model']
    print(f'Generating seismic data for shot {ind}...')
    generate_shot_data_time(shot, solver, model)
    tend = timer()
    print(f'Generating seismic data for shot {ind}... ok. {tend-tstart:.3g}s')
    return dict_shot

def _generate_shot_data_time_pool(shots, solver, model, ncores):
    arr_shots = []
    for ind, shot in enumerate(shots):
        arr_shots.append({'ind': ind,
                          'shot': shot,
                          'solver': solver,
                          'model': model,})
    tstart = timer()
    with Pool(processes=ncores) as pool:
        async_result = pool.map_async(_generate_shot_data_time_shot, arr_shots)
        arr_res = async_result.get()
    tend = timer()
    print(f'Generate seismic data for all shots: {tend-tstart:.3g}s')

    shots = []
    for res in arr_res:
        shots.append(res['shot'])
    
    solver = arr_res[0]['solver']
    return shots, solver

def generate_seismic_data(shots, solver, model, verbose=False, frequencies=None, save_method=None, ncores=None, **kwargs):
    """Given a list of shots and a solver, generates seismic data.

    Parameters
    ----------
    shots : list of pysit.Shot
        Collection of shots to be processed
    solver : pysit.WaveSolver
        Instance of wave solver to be used.
    **kwargs : dict, optional
        Optional arguments.

    Notes
    -----
    `kwargs` may be used to specify `C0` and `wavefields` to    `generate_shot_data`.

    """
    
    if verbose:
        print('Generating data...')
        tt = time.time()

    if solver.supports['equation_dynamics'] == "time":
        # breakpoint()
        if ncores == 1:
            for shot in shots:
                generate_shot_data_time(shot, solver, model, verbose=verbose, **kwargs)
        else:
            if ncores is None:
                ncores = cpu_count()
            print(f'\nGenerating seismic data in parallel using {ncores} cores')
            shots, solver = _generate_shot_data_time_pool(shots, solver, model, ncores) 

    elif solver.supports['equation_dynamics'] == "frequency":
        if frequencies is None:
            raise TypeError('A frequency solver is passed, but no frequencies are given')
        elif 'petsc' in kwargs and kwargs['petsc'] is not None:
            # solve the Helmholtz operator for several rhs                
            generate_shot_data_frequency_list(shots, solver, model, frequencies, verbose=verbose, **kwargs)
        else:
            for shot in shots:            
                generate_shot_data_frequency(shot, solver, model, frequencies, verbose=verbose, **kwargs)
    else:
        raise TypeError("A time or frequency solver must be specified.")

    if verbose:
        data_tt = time.time() - tt
        print('Data generation: {0}s'.format(data_tt))
        print('Data generation: {0}s/shot'.format(data_tt/len(shots)))

    if verbose:
        print('Saving data...')
        tt = time.time()
    if solver.supports['equation_dynamics'] == "time":
        # store shots in memory for a next simulation
        if save_method is not None:
            if save_method=='pickle':
                try:
                    import pickle
                except ImportError:
                    raise ImportError('cPickle is not installed please install it and try again')

                newpath = r'./shots_time'
                if os.path.exists(newpath):
                    shutil.rmtree(newpath)

                if not os.path.exists(newpath):
                    os.makedirs(newpath)

                for i in range(len(shots)):
                    fshot =newpath+'/shot_'+str(i+1)+'.save'
                    f = open(fshot, 'wb')
                    pickle.dump(shots[i].receivers.data,f, protocol=pickle.HIGHEST_PROTOCOL)
                    f.close()
                    fts =newpath+'/ts_'+str(i+1)+'.save'
                    f = open(fts, 'wb')
                    pickle.dump(shots[i].receivers.ts,f, protocol=pickle.HIGHEST_PROTOCOL)
                    f.close()
            elif save_method=='savemat':
                try:
                    import scipy.io as io
                except ImportError:
                    raise ImportError('scipy.io is not installed please install it and try again')

                newpath = r'./shots_time'
                if os.path.exists(newpath):
                    shutil.rmtree(newpath)

                if not os.path.exists(newpath):
                    os.makedirs(newpath)

                for i in range(len(shots)):
                    fshot =newpath+'/shot_'+str(i+1)+'.mat'
                    io.savemat(fshot,mdict={fshot:shots[i].receivers.data})
                    fts =newpath+'/ts_'+str(i+1)+'.mat'
                    io.savemat(fts,mdict={fts:shots[i].receivers.ts})
            elif save_method=='h5py':
                try:
                    import h5py
                except ImportError:
                    raise ImportError('h5py is not installed please install it and try again')

                newpath = r'./shots_time'

                if os.path.exists(newpath):
                    shutil.rmtree(newpath)
                
                if not os.path.exists(newpath):
                    os.makedirs(newpath)

                for i in range(len(shots)):
                    fshot =newpath+'/shot_'+str(i+1)+'.hdf5'
                    f = h5py.File(fshot,"w")
                    dts = f.create_dataset(fshot,shots[i].receivers.data.shape,shots[i].receivers.data.dtype)
                    dts[...] = shots[i].receivers.data
                    f.close()
                    fts =newpath+'/ts_'+str(i+1)+'.hdf5'
                    f = h5py.File(fts,'w')
                    dts = f.create_dataset(fts,shots[i].receivers.ts.shape,shots[i].receivers.ts.dtype)
                    dts[...] = shots[i].receivers.ts
                    f.close()
            else:
                raise TypeError('Unknown save_method')
    if solver.supports['equation_dynamics'] == "frequency":
        # store shots in memory for a next simulation
        if save_method is not None:
            if save_method=='pickle':
                try:
                    import pickle
                except ImportError:
                    raise ImportError('cPickle is not installed please install it and try again')

                newpath = r'./shots_frequency'
                if os.path.exists(newpath):
                    shutil.rmtree(newpath)

                if not os.path.exists(newpath):
                    os.makedirs(newpath)

                for i in range(len(shots)):
                    fshot =newpath+'/shot_'+str(i+1)+'.save'
                    f = file(fshot, 'wb')
                    pickle.dump(shots[i].receivers.data_dft,f, protocol=pickle.HIGHEST_PROTOCOL)
                    f.close()
            elif save_method=='savemat':
                try:
                    import scipy.io as io
                except ImportError:
                    raise ImportError('scipy.io is not installed please install it and try again')

                newpath = r'./shots_frequency'
                if os.path.exists(newpath):
                    shutil.rmtree(newpath)

                if not os.path.exists(newpath):
                    os.makedirs(newpath)

                for i in range(len(shots)):
                    data_frequency = shots[i].receivers.data_dft
                    for nu in data_frequency:
                        fshot =newpath+'/shot_'+str(i+1)+'_nu_'+str(nu)+'.mat'
                        io.savemat(fshot,mdict={fshot:shots[i].receivers.data_dft[nu]})
            elif save_method=='h5py':
                raise NotImplementedError('h5py is not efficient with frequency data please use another data container')
            else:
                raise TypeError('Unknown save_method')
    if verbose:
        save_tt = time.time() - tt
        print('Data saving: {0}s'.format(save_tt))
        print('Data saving: {0}s/shot'.format(save_tt/len(shots)))
    
    return shots, solver

def generate_shot_data_time(shot, solver, model, wavefields=None, wavefields_padded=None, verbose=False, **kwargs):
    """Given a shots and a solver, generates seismic data at the specified
    receivers.

    Parameters
    ----------
    shots : list of pysit.Shot
        Collection of shots to be processed
    solver : pysit.WaveSolver
        Instance of wave solver to be used.
    model_parameters : solver.ModelParameters, optional
        Wave equation parameters used for generating data.
    wavefields : list, optional
        List of wave states.
    verbose : boolean
        Verbosity flag.

    Notes
    -----
    An empty list passed as `wavefields` will be populated with the state of the wave
    field at each time index.

    """

    solver.model_parameters = model

    # Ensure that the receiver data is empty.  And that interpolator is setup.
    # shot.clear_data(solver.nsteps)
    ts = solver.ts()
    shot.reset_time_series(ts)

    # Populate some stuff that will probably not exist soon anyway.
    shot.dt = solver.dt
    shot.trange = solver.trange

    if solver.supports['equation_dynamics'] != "time":
        raise TypeError('Solver must be a time solver to generate data.')

    if(wavefields is not None):
        wavefields[:] = []
    if(wavefields_padded is not None):
        wavefields_padded[:] = []

    #Frequently used local references are faster than remote references
    mesh = solver.mesh
    dt = solver.dt
    source = shot.sources

    # Step k = 0
    # p_0 is a zero array because if we assume the input signal is causal and we
    # assume that the initial system (i.e., p_(-2) and p_(-1)) is uniformly
    # zero, then the leapfrog scheme would compute that p_0 = 0 as well.
    # This is stored in this solver specific data structure.
    solver_data = solver.SolverData()

    rhs_k   = np.zeros(mesh.shape(include_bc=True))
    rhs_kp1 = np.zeros(mesh.shape(include_bc=True))

    # k is the t index.  t = k*dt.
    for k in range(solver.nsteps):
#       print "  Computing step {0}...".format(k)
        uk = solver_data.k.primary_wavefield

        # Extract the primary, non ghost or boundary nodes from the wavefield
        # uk_bulk is a view so no new storage
        # also, this is a bad way to do things.  Somehow the BC padding needs to be better hidden.  Maybe it needs to always be a part of the mesh?
        uk_bulk = mesh.unpad_array(uk)

        # Record the data at t_k
        shot.receivers.sample_data_from_array(uk_bulk, k, **kwargs)

        if(wavefields is not None):
            wavefields.append(uk_bulk.copy())
        if(wavefields_padded is not None):
            wavefields_padded.append(uk.copy())

        # When k is the nth step, the next time step is not needed, so save
        # computation and break out early.
        if(k == (solver.nsteps-1)): break

        if k == 0:
            rhs_k = mesh.pad_array(source.f(k*dt), out_array=rhs_k)
            rhs_kp1 = mesh.pad_array(source.f((k+1)*dt), out_array=rhs_kp1)
        else:
            # shift time forward
            rhs_k, rhs_kp1 = rhs_kp1, rhs_k
            rhs_kp1 = mesh.pad_array(source.f((k+1)*dt), out_array=rhs_kp1)

        # Given the state at k and k-1, compute the state at k+1
        solver.time_step(solver_data, rhs_k, rhs_kp1)

        # Don't know what data is needed for the solver, so the solver data
        # handles advancing everything forward by one time step.
        # k-1 <-- k, k <-- k+1, etc
        solver_data.advance()