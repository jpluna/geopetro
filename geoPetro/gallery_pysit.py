# Std import block
###
import os
import numpy as np
import matplotlib.pyplot as plt
import time 
import shutil
import csv
from timeit import default_timer as timer
from pathlib import Path
from scipy.interpolate import interp1d
from obspy import read


import pysit
from pysit import *
from pysit.gallery import horizontal_reflector
from pysit.gallery import bp, marmousi

import geoPetro.gallery_base as glrb
# import gallery_base as glrb
###

def get_models_pysit(case_name, from_gallery=True, custom_pars=None):
    dict_data = glrb.read_params_file(case_name, from_gallery, custom_pars)

    water_depth = dict_data['water_depth']
    physical_origin = dict_data['physical_origin']
    base_physical_size = dict_data['base_physical_size']
    physical_dimensions_units = dict_data['physical_dimensions_units']
    base_pixels = dict_data['base_pixels']
    base_pixel_scale = dict_data['base_pixel_scale']
    base_pixel_units = dict_data['base_pixel_units']
    factor = dict_data['factor']

    file_vel = dict_data['file_vel']
    file_vel0 = dict_data['file_vel0']
    transpose_data = bool(dict_data['transpose_data'])
    if dict_data['type_data'] == 'float32':
        type_data = np.float32
    elif dict_data['type_data'] == 'float':
        type_data = float
    else:
        raise ValueError('Invalid file type')

    coordinate_lbounds = physical_origin
    coordinate_rbounds = physical_origin + base_physical_size

    pixel_scale = 1/factor * base_pixel_scale
    length = coordinate_rbounds - coordinate_lbounds
    pixels = np.ceil(length/pixel_scale).astype(int)
    # Update physical size
    physical_size = pixel_scale * pixels
    pixels += 1 # include last point in all dimensions

    # extract or default the boundary conditions
    x_lbc = PML(0.1*physical_size[0], 100.0)
    x_rbc = PML(0.1*physical_size[0], 100.0)
    z_lbc = PML(0.1*physical_size[1], 100.0)
    z_rbc = PML(0.1*physical_size[1], 100.0)

    x_config = (coordinate_lbounds[0], coordinate_rbounds[0], x_lbc, x_rbc)
    z_config = (coordinate_lbounds[1], coordinate_rbounds[1], z_lbc, z_rbc)
    d = RectangularDomain(x_config, z_config)

    m = CartesianMesh(d, *pixels)

    config = {  'base_physical_origin': physical_origin, 
                'base_physical_size': base_physical_size,
                'base_pixel_scale': base_pixel_scale,
                'base_pixels': base_pixels,
                'pixel_scale': pixel_scale,
                'pixels': pixels
                }

    print('Loading models...')
    tstart = timer()
    vp = glrb.read_model(file_vel, type_data=type_data)
    if transpose_data:
        vp = vp.T
    if vp.shape[0] == vp.size:
        # In this case, data was stored as a column vector
        # So we need to reshape it
        vp = vp.reshape(base_pixels)
    vp = glrb.resample_data(vp, config)

    vp0 = glrb.read_model(file_vel0, type_data=type_data)
    if transpose_data:
        vp0 = vp0.T
    if vp0.shape[0] == vp0.size:
        # In this case, data was stored as a column vector
        # So we need to reshape it
        vp0 = vp0.reshape(base_pixels)
    vp0 = glrb.resample_data(vp0, config)

    grid = m.mesh_coords()
    ZZ = grid[-1].reshape(vp.shape)
    loc = np.where(ZZ < water_depth)
    vp0[loc] = vp[loc]

    C = vp.reshape(m.shape())
    C0 = vp0.reshape(m.shape())
    tend = timer()
    print(f'Loading models... {tend-tstart}s')

    res = {'dict_data': dict_data, 
           'C': C, 
           'C0': C0, 
           'd': d, 
           'm': m}
    return res

def get_shots_pysit(res):
    dict_data = res['dict_data']

    freq = dict_data['frequency']
    trange = dict_data['trange']

    nSources = dict_data['nsources']
    source_depth = dict_data['source_depth']

    if 'receiver_depth' in dict_data: 
        receiver_depth = dict_data['receiver_depth']
        print('setting receiver_depth from dictionary')
    else: 
        receiver_depth = source_depth 
        print('setting receiver_depth as source_depth')
    
    if 'nreceivers' in dict_data:
        nreceivers = dict_data['nreceivers']
    else:
        nreceivers = 'max'

    # Set up shots
    tstart = timer()
    print('Setting up shots...')
    shots = equispaced_acquisition(res['m'],
                                   RickerWavelet(freq),
                                   sources=nSources,
                                   source_depth=source_depth,
                                   source_kwargs={},
                                   receivers=nreceivers, 
                                   receiver_depth=receiver_depth,
                                   receiver_kwargs={},
                                   )
    tend = timer()
    print(f'Setting up shots... {tend-tstart}')

    # Define and configure the wave solver
    # trange = (0.0, 3.0)
    tstart = timer()
    print('Configuring the wave solver...')
    solver = ConstantDensityAcousticWave(res['m'],
                                         spatial_accuracy_order=6,
                                         trange=trange,
                                         kernel_implementation='cpp')
    tend = timer()
    print(f'Configuring the wave solver... {tend-tstart}')

    # Generate synthetic Seismic data
    print('Data generation...')
    tstart = timer()
    base_model = solver.ModelParameters(res['m'],{'C': res['C']})
    wavefields = []
    generate_seismic_data(shots, solver, base_model, wavefields=wavefields, save_method='pickle', verbose=True)

    resModel = {'m':res['m'], 
                'C':res['C'], 
                'C0':res['C0'], 
                'solver':solver, 
                'shots':shots}
    return resModel