###
import  geoPetro.myobjt as myobjt
import numpy as np
import ot

###
## additional functions
def smooth(x, tau=1e-4, kernel='a'):
    if kernel == 'a':
        aux =  1 + np.exp(-x / tau)
        return x + tau * np.log(aux), 1.0/ aux
    elif kernel == 'b':
        aux = np.sqrt( x * x + 4 * tau * tau)
        return (x + aux) / 2, (1 + (x / aux)) / 2
    else:
        raise ValueError('Invalid kernel')
    
def optTransportCost(gamma,xm, ym, M, reg, regm):
    gamma1 = gamma.sum(axis=1)
    gammaT1 = gamma.sum(axis=0)
    val = (gamma * M).sum() +  reg * (gamma1 * (np.log(gamma1/ xm)-1) + xm).sum() + reg * (gammaT1 * (np.log(gammaT1/ym)-1) + ym).sum()
    return val




def defineRm(a,b):
    am = a.sum()
    bm = b.sum()
    aux = am/bm if am/bm >1 else bm/am
    if aux < 1.01:
        rm = 100
    elif aux<1.5:
        rm = 10
    elif aux < 2:
        rm =5
    else:
        rm = 1e-3

    return rm


def uotS(x,y, M, reg):
    vals = []
    rmAx1 = np.zeros_like(x) # Ax \times ones
    regm = defineRm(x, y)
    for i, (xm, ym) in enumerate((x,y), (x,x), (y,y)):
        gamma, dic = ot.unbalanced.sinkhorn_stabilized_unbalanced(xm, ym, M, reg, regm, log=True) 
        vals.append( optTransportCost(gamma,xm, ym, M, reg, regm))
        if i == 0:
            rmAx1 -= gamma.sum(axis=1)
        elif i == 1:
            rmAx1 += 0.5 *(gamma.sum(axis=0) + gamma.sum(axis=1)) 
    rmAx1 = regm * (rmAx1/x)
    val = vals[0] - 0.5 * sum(vals[1:]) + (reg*0.5* ((x.sum() - y.sum())**2))
    
    return val, rmAx1



###

class myOls(myobjt.myObj):
    def __myInit__(self): 
        # Just because all other functions have an init
        pass

    def myMisfit(self, simSeis, obsSeis, dt): 
        '''
        the result should be scaled with dt
        '''
        val =  0.5 * ((simSeis - obsSeis) ** 2).sum() * dt
        return val

    def myAdjSource(self, simSeis, obsSeis, dt): 
        '''
        the result should NOT be scaled with dt
        return: val, adjSource
        '''
        r = (simSeis - obsSeis)
        val =  0.5 * (r * r).sum() * dt
        return val, -r 

class myOTx(myobjt.myObj):

    def __myInit__(self, shots): 
        ss= shots[0] 
        positions = [rr.position for rr in ss.receivers.receiver_list]
        coords = np.array(positions)
        self.otM = ot.dist(coords,coords,'sqeuclidean')
        self.transl = 1
        print('myInit done')


    def myMisfit(self, simSeis, obsSeis, dt): 
        '''
        the result should be scaled with dt
        ''' 
        # computing OT distance


        M = self.otM
        trs = self.transl

        factor = M.max() 
        M = M/factor
        reg=2.e+0/factor
        regm=1.e-8
        val = 0


        reg = 1
        regm = 1e-2





        # for sim, obs in zip(simSeis, obsSeis): 
        for i, (sim, obs) in enumerate(zip(simSeis, obsSeis)): 
            # print('f',i)
            gamma,dic=ot.unbalanced.sinkhorn_knopp_unbalanced(sim + trs, obs + trs, M, reg, regm, log=True) 
            val += (gamma*M).sum() 

        return val*dt

    def myAdjSource(self, simSeis, obsSeis, dt): 
        '''
        the result should NOT be scaled with dt
        return: val, adjSource
        '''

        # breakpoint()

        M = self.otM
        trs = self.transl

        factor = M.max() 
        M = M/factor
        reg=2.e+0/factor
        regm=1.e-8




        reg = 1
        regm = 1e-2






        val = 0
        adjS = np.empty_like(simSeis)

        for i, (sim, obs) in enumerate(zip(simSeis, obsSeis)): 
            # print(i)
            gamma, dic = ot.unbalanced.sinkhorn_knopp_unbalanced(sim + trs, obs + trs, M, reg, regm, log=True) 
            adjS[i,:] = regm * (1 -  gamma.sum(axis=1)/(sim + trs))
            val += (gamma * M).sum()
        # print('adjSource')
        breakpoint()
        # return val * dt, -adjS
        return val * dt, adjS

class myOTt(myobjt.myObj):

    # def __myInit__(self, shots): 
    def __myInit__(self): 
        # ss= shots[0] 
        # positions = [rr.position for rr in ss.receivers.receiver_list]
        # coords = np.array(positions)
        # self.otM = ot.dist(coords,coords,'sqeuclidean')
        self.otM = None
        self.transl = 10.0
        print('myInit done')


    def myMisfit(self, simSeis, obsSeis, dt): 
        '''
        the result should be scaled with dt
        ''' 
        # computing OT distance

        nT = simSeis.shape[0]
        # print(100*'=')
        # print('Juanpa= nT= {}'.format(nT))
        if ((self.otM is None) or (nT > len(self.otM))):
            coords = np.arange(int(nT * 1.2)).reshape(-1,1)
            self.otM = ot.dist(coords,coords,'sqeuclidean')

        M = self.otM[:nT,:nT]

        trs = self.transl

        factor = M.max() 
        M = M/factor
        reg=2.e+0/factor
        regm=1.e-8
        val = 0

        # for sim, obs in zip(simSeis, obsSeis): 
        for i, (sim, obs) in enumerate(zip(simSeis.T, obsSeis.T)): 
            gamma,dic=ot.unbalanced.sinkhorn_knopp_unbalanced(sim + trs, obs + trs, M, reg, regm, log=True) 
            val += (gamma*M).sum() 
        return val * dt * dt

    def myAdjSource(self, simSeis, obsSeis, dt): 
        '''
        the result should NOT be scaled with dt
        return: val, adjSource
        '''

        nT = simSeis.shape[0]
        if ((self.otM is None) or (nT > len(self.otM))):
            # coords = np.arange(int(nT * 1.2))
            coords = np.arange(int(nT * 1.2)).reshape(-1,1)
            self.otM = ot.dist(coords,coords,'sqeuclidean')

        M = self.otM[:nT,:nT]

        trs = self.transl

        factor = M.max() 
        M = M/factor
        reg=2.e+0/factor
        regm=1.e-8
        val = 0

        val = 0
        adjS = np.empty_like(simSeis)

        for i, (sim, obs) in enumerate(zip(simSeis.T, obsSeis.T)): 
            gamma, dic = ot.unbalanced.sinkhorn_knopp_unbalanced(sim + trs, obs + trs, M, reg, regm, log=True) 
            adjS[:, i] = regm * (1 -  gamma.sum(axis=1)/(sim + trs))
            val += (gamma * M).sum()
        # print('adjSource')
        return val * dt * dt,  -adjS

class KLx(myobjt.myObj):

    def __myInit__(self, kernel='b', tau=1e-4 ): 
        self.kernel=kernel
        self.tau = tau
        print('smoothing kernet set to {}'.format(kernel))
        print('tau set to {}'.format(tau))

    def myMisfit(self, simSeis, obsSeis, dt): 
        '''
        the result should be scaled with dt
        '''
        xMass1, _ = smooth(simSeis,tau=self.tau, kernel=self.kernel)
        xMass2, _ = smooth(-obsSeis,tau=self.tau, kernel=self.kernel)
        xMass = xMass1 + xMass2
        del xMass1 
        del xMass2
        yMass1, _ = smooth(-simSeis,tau=self.tau,kernel=self.kernel)
        yMass2, _ = smooth(obsSeis,tau=self.tau, kernel=self.kernel)

        yMass = yMass1 + yMass2
        del yMass1 
        del yMass2

        val = (xMass * np.log(xMass/yMass) - xMass + yMass).sum()  * dt

        return val

    def myAdjSource(self, simSeis, obsSeis, dt): 
        '''
        the result should NOT be scaled with dt
        return: val, adjSource
        '''

        xMass1, dxMass = smooth(simSeis,tau=self.tau, kernel=self.kernel)
        xMass2, _ = smooth(-obsSeis,tau=self.tau, kernel=self.kernel)
        xMass = xMass1 + xMass2
        del xMass1 
        del xMass2
        yMass1, dyMass = smooth(-simSeis,tau=self.tau,kernel=self.kernel)
        yMass2, _  = smooth(obsSeis,tau=self.tau, kernel=self.kernel)

        yMass = yMass1 + yMass2
        del yMass1 
        del yMass2
        xDivy = xMass/yMass

        val = (xMass * np.log(xDivy) - xMass + yMass).sum()  * dt
        r = np.log(xDivy) * dxMass - (1 -  xDivy) * dyMass

        return val, -r 


class KLx2(myobjt.myObj):

    def __myInit__(self, kernel='b', tau=1e-4 ): 
        self.kernel=kernel
        self.tau = tau
        print('smoothing kernet set to {}'.format(kernel))
        print('tau set to {}'.format(tau))

    def myMisfit(self, simSeis, obsSeis, dt): 
        '''
        the result should be scaled with dt
        '''
        xMassPv, _ = smooth(simSeis,tau=self.tau, kernel=self.kernel)
        xMassP0, _ = smooth(obsSeis,tau=self.tau, kernel=self.kernel)

        mxMassPv, _ = smooth(-simSeis,tau=self.tau, kernel=self.kernel)
        mxMassP0, _ = smooth(-obsSeis,tau=self.tau, kernel=self.kernel)

        val = (xMassPv * np.log(xMassPv/xMassP0) - xMassPv + xMassP0).sum()  * dt
        val += (mxMassPv * np.log(mxMassPv/mxMassP0) - mxMassPv + mxMassP0).sum()  * dt
        return val

    def myAdjSource(self, simSeis, obsSeis, dt): 
        '''
        the result should NOT be scaled with dt
        return: val, adjSource
        '''

        xMassPv, dxMassPv = smooth(simSeis,tau=self.tau, kernel=self.kernel)
        xMassP0, _ = smooth(obsSeis,tau=self.tau, kernel=self.kernel)

        mxMassPv, dmxMassPv = smooth(-simSeis,tau=self.tau, kernel=self.kernel)
        mxMassP0, _ = smooth(-obsSeis,tau=self.tau, kernel=self.kernel)

        val = (xMassPv * np.log(xMassPv/xMassP0) - xMassPv + xMassP0).sum()  * dt
        val += (mxMassPv * np.log(mxMassPv/mxMassP0) - mxMassPv + mxMassP0).sum()  * dt


        xDiv0 = xMassPv/xMassP0
        mxDiv0 = mxMassPv/mxMassP0

        r = np.log(xDiv0) * dxMassPv - np.log(mxDiv0) * dmxMassPv

        return val, -r 





class UOTx(myobjt.myObj):

    def __myInit__(self, shots, kernel='b'): 
        self.kernel = kernel
        ss= shots[0] 
        positions = [rr.position for rr in ss.receivers.receiver_list]
        coords = np.array(positions)
        self.otM = ot.dist(coords,coords,'sqeuclidean')
        self.transl = 1
        print('myInit done')

    def myMisfit(self, simSeis, obsSeis, dt): 
        '''
        the result should be scaled with dt
        ''' 
        # computing OT distance


        M = self.otM

        factor = M.max() 
        M = M/factor
        # reg=2.e+0/factor
        reg=1.0
        # regm=1.e-8


        xMass1, dxMass = smooth(simSeis, kernel=self.kernel)
        xMass2, _ = smooth(-obsSeis, kernel=self.kernel)
        xMass = xMass1 + xMass2
        del xMass1 
        del xMass2
        yMass1, dyMass = smooth(-simSeis,kernel=self.kernel)
        yMass2, _  = smooth(obsSeis, kernel=self.kernel)

        yMass = yMass1 + yMass2
        del yMass1 
        del yMass2

        val = 0

        for i, (xm, ym) in enumerate(zip(xMass, yMass)): 
            regm = defineRm(xm, ym)
            gamma, dic = ot.unbalanced.sinkhorn_stabilized_unbalanced(xm, ym, M, reg, regm, log=True) 
            gamma1 = gamma.sum(axis=1)
            gammaT1 = gamma.sum(axis=0)
            val += (gamma * M).sum() +  reg * (gamma1 * (np.log(gamma1/ xm)-1) + xm).sum() + reg * (gammaT1 * (np.log(gammaT1/ym)-1) + ym).sum()
        # print('adjSource')
        return val * dt

    def myAdjSource(self, simSeis, obsSeis, dt): 
        '''
        the result should NOT be scaled with dt
        return: val, adjSource
        '''

        M = self.otM

        factor = M.max() 
        M = M/factor
        reg=1 
        regm=1.e-8


        xMass1, dxMass = smooth(simSeis, kernel=self.kernel)
        xMass2, _ = smooth(-obsSeis, kernel=self.kernel)
        xMass = xMass1 + xMass2
        del xMass1 
        del xMass2
        yMass1, dyMass = smooth(-simSeis,kernel=self.kernel)
        yMass2, _  = smooth(obsSeis, kernel=self.kernel)

        yMass = yMass1 + yMass2
        del yMass1 
        del yMass2


        val = 0
        adjS = np.empty_like(simSeis)

        for i, (xm, ym) in enumerate(zip(xMass, yMass)): 
            regm = defineRm(xm, ym)
            gamma, dic = ot.unbalanced.sinkhorn_stabilized_unbalanced(xm, ym, M, reg, regm, log=True) 

            adjS[i,:] = reg * ( (1 -  gamma.sum(axis=1)/(xm)) * dxMass[i] - (1 -  gamma.sum(axis=0)/(ym)) * dyMass[i]  )

            gamma1 = gamma.sum(axis=1)
            gammaT1 = gamma.sum(axis=0)
            val += (gamma * M).sum() +  reg * (gamma1 * (np.log(gamma1/ xm)-1) + xm).sum() + reg * (gammaT1 * (np.log(gammaT1/ym)-1) + ym).sum()
        # print('adjSource')
        return val * dt, -adjS

class UOTx2(myobjt.myObj):
    tau = None

    def __myInit__(self, shots, kernel='b', tau=1e-4): 
        self.kernel = kernel
        ss= shots[0] 
        positions = [rr.position for rr in ss.receivers.receiver_list]
        coords = np.array(positions)
        self.otM = ot.dist(coords,coords,'sqeuclidean')
        self.transl = 1
        self.tau = tau
        print('myInit done')


    def myMisfit(self, simSeis, obsSeis, dt): 
        '''
        the result should be scaled with dt
        ''' 
        # computing OT distance


        M = self.otM

        factor = M.max() 
        M = M/factor
        # reg=2.e+0/factor
        reg=1.0
        # regm=1.e-8


        xMassPv, _ = smooth(simSeis,tau=self.tau, kernel=self.kernel)
        xMassP0, _ = smooth(obsSeis,tau=self.tau, kernel=self.kernel)

        mxMassPv, _ = smooth(-simSeis,tau=self.tau, kernel=self.kernel)
        mxMassP0, _ = smooth(-obsSeis,tau=self.tau, kernel=self.kernel)

        val = 0
        for pv, p0 in [(xMassPv, xMassP0), (mxMassPv, mxMassP0)]:

            for i, (xm, ym) in enumerate(zip(pv, p0)): 
                regm = defineRm(xm, ym)
                gamma, dic = ot.unbalanced.sinkhorn_stabilized_unbalanced(xm, ym, M, reg, regm, log=True) 
                gamma1 = gamma.sum(axis=1)
                gammaT1 = gamma.sum(axis=0)
                val += (gamma * M).sum() +  reg * (gamma1 * (np.log(gamma1/ xm)-1) + xm).sum() + reg * (gammaT1 * (np.log(gammaT1/ym)-1) + ym).sum()
        # print('adjSource')
        return val * dt



    def myAdjSource(self, simSeis, obsSeis, dt): 
        '''
        the result should NOT be scaled with dt
        return: val, adjSource
        '''

        M = self.otM

        factor = M.max() 
        M = M/factor
        reg=1 
        regm=1.e-8

        xMassPv, dxMassPv =  smooth(simSeis,tau=self.tau, kernel=self.kernel)
        xMassP0, _ = smooth(obsSeis,tau=self.tau, kernel=self.kernel)

        mxMassPv, mdxMassPv = smooth(-simSeis,tau=self.tau, kernel=self.kernel)
        mxMassP0, _ = smooth(-obsSeis,tau=self.tau, kernel=self.kernel)

        val = 0
        adjS = np.zeros_like(simSeis)

        for k , (pv, p0) in enumerate([(xMassPv, xMassP0), (mxMassPv, mxMassP0)]):

            for i, (xm, ym) in enumerate(zip(pv, p0)): 
                regm = defineRm(xm, ym)
                gamma, dic = ot.unbalanced.sinkhorn_stabilized_unbalanced(xm, ym, M, reg, regm, log=True) 
                gamma1 = gamma.sum(axis=1)
                gammaT1 = gamma.sum(axis=0)
                val += (gamma * M).sum() +  reg * (gamma1 * (np.log(gamma1/ xm)-1) + xm).sum() + reg * (gammaT1 * (np.log(gammaT1/ym)-1) + ym).sum()
                
                adjS[i,:] += ( -1 if k>0 else 1) * reg * ( (1 -  gamma.sum(axis=1)/(xm)) * (mdxMassPv[i]  if k>0 else dxMassPv[i]) ) 

        # print('adjSource')
        return val * dt, -adjS



class UOTSx(myobjt.myObj):

    def __myInit__(self, shots, kernel='b'): 
        self.kernel = kernel
        ss= shots[0] 
        positions = [rr.position for rr in ss.receivers.receiver_list]
        coords = np.array(positions)
        self.otM = ot.dist(coords,coords,'sqeuclidean')
        self.transl = 1
        print('myInit done')

    def myMisfit(self, simSeis, obsSeis, dt): 
        '''
        the result should be scaled with dt
        ''' 
        # computing OT distance
        M = self.otM

        factor = M.max() 
        M = M/factor
        # reg=2.e+0/factor
        reg=1.0
        # regm=1.e-8

        xMass1, dxMass = smooth(simSeis, kernel=self.kernel)
        xMass2, _ = smooth(-obsSeis, kernel=self.kernel)
        xMass = xMass1 + xMass2
        del xMass1 
        del xMass2
        yMass1, dyMass = smooth(-simSeis,kernel=self.kernel)
        yMass2, _  = smooth(obsSeis, kernel=self.kernel)

        yMass = yMass1 + yMass2
        del yMass1 
        del yMass2

        val = 0

        for i, (xm, ym) in enumerate(zip(xMass, yMass)): 
            val += uotS(xm, ym, M, reg)[0]

        return val * dt

    def myAdjSource(self, simSeis, obsSeis, dt): 
        '''
        the result should NOT be scaled with dt
        return: val, adjSource
        '''


        M = self.otM

        factor = M.max() 
        M = M/factor
        reg=1.0

        xMass1, dxMass = smooth(simSeis, kernel=self.kernel)
        xMass2, _ = smooth(-obsSeis, kernel=self.kernel)
        xMass = xMass1 + xMass2
        del xMass1 
        del xMass2
        yMass1, dyMass = smooth(-simSeis,kernel=self.kernel)
        yMass2, _  = smooth(obsSeis, kernel=self.kernel)

        yMass = yMass1 + yMass2
        del yMass1 
        del yMass2

        val = 0
        adjS = np.empty_like(simSeis)

        for i, (xm, ym) in enumerate(zip(xMass, yMass)): 
            val += uotS(xm, ym, M, reg)[0]
            # adjS[i,:] = reg * ( (1 -  gamma.sum(axis=1)/(xm)) * dxMass[i] - (1 -  gamma.sum(axis=0)/(ym)) * dyMass[i]  )
            # precisa ser programada a adjunta

        return val * dt, -adjS



class UOTSx2(myobjt.myObj):
    tau = None

    def __myInit__(self, shots, kernel='b', tau=1e-4): 
        self.kernel = kernel
        ss= shots[0] 
        positions = [rr.position for rr in ss.receivers.receiver_list]
        coords = np.array(positions)
        self.otM = ot.dist(coords,coords,'sqeuclidean')
        self.transl = 1
        self.tau = tau
        print('myInit done')



    def myMisfit(self, simSeis, obsSeis, dt): 
        '''
        the result should be scaled with dt
        ''' 
        # computing OT distance

        M = self.otM

        factor = M.max() 
        M = M/factor
        reg=1 
        regm=1.e-8

        xMassPv, _ =  smooth(simSeis,tau=self.tau, kernel=self.kernel)
        xMassP0 = (np.abs(obsSeis) + obsSeis)/2
        mxMassP0 = xMassP0 - obsSeis

        mxMassPv = xMassPv -simSeis 

        val = 0

        for k , (pv, p0) in enumerate([(xMassPv, xMassP0), (mxMassPv, mxMassP0)]):
            for i, (xm, ym) in enumerate(zip(pv, p0)): 
                
                valAux, _ = uotS(xm,ym, M, reg)
                val += valAux

        # print('adjSource')
        return val * dt



    def myAdjSource(self, simSeis, obsSeis, dt): 
        '''
        the result should NOT be scaled with dt
        return: val, adjSource
        '''

        M = self.otM

        factor = M.max() 
        M = M/factor
        reg=1 
        regm=1.e-8

        xMassPv, dxMassPv =  smooth(simSeis,tau=self.tau, kernel=self.kernel)
        xMassP0 = (np.abs(obsSeis) + obsSeis)/2
        mxMassP0 = xMassP0 - obsSeis

        mxMassPv = xMassPv -simSeis 

        val = 0
        adjS = np.zeros_like(simSeis)

        for k , (pv, p0, dv) in enumerate([(xMassPv, xMassP0, dxMassPv), (mxMassPv, mxMassP0, dxMassPv-1)]):
            for i, (xm, ym,dx) in enumerate(zip(pv, p0, dx)): 
                
                valAux, rmAx1 = uotS(xm,ym, M, reg)
                val += valAux
                adjS[i,:] +=  (rmAx1 + reg*(xm.sum() - ym.sum())) * dx

        # print('adjSource')
        return val * dt, -adjS
    

