###
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import geoPetro.myDevito as myDevito
from  . import  misfitGallery as mg
from . import gallery_devito as glr

###
class devitoModel():
    model0=None
    model1=None
    dict_pars=None
    geometry0=None
    geometry1=None
    slices= None

    def __init__(self, modelKey, nSources=1, custom_pars=None ): 
        model1, model0, dict_pars = glr.get_models_devito(modelKey, from_gallery=True, custom_pars=custom_pars) 
        dict_pars['nsources'] = nSources
        geometry1, geometry0 = glr.get_geometry_devito(dict_pars, model1, model0)
        self.model0 = model0
        self.model1 = model1
        self.geometry0=geometry0
        self.geometry1=geometry1

        self.dict_pars = dict_pars
        ##  defining slices for referencing borders
        nbl = model0.nbl
        self.slices = 2*[slice(nbl, -nbl)]

###


###

class toy():
    model = None
    bb=None

    def __init__(self, modelKey, nSources=1, custom_pars=None): 
        self.model = devitoModel(modelKey, nSources=nSources, custom_pars=custom_pars)

    def getEmptyVector(self):
        return np.array(self.model.model0.vp.data[self.model.slices]).reshape(-1,)
    
    def getC0(self):
        return np.array(self.model.model0.vp.data[self.model.slices]).reshape(-1,)

    def getC(self):
        return np.array(self.model.model1.vp.data[self.model.slices]).reshape(-1,)
  
    def setObjective(self, misfit='l2', ncores=4):
        # availableKeys=['l2', 'klx', 'otx','uotx', 'uotx2', 'uotsx', 'uotsx2']
        availableKeys=['l2', 'klx']
        if isinstance(misfit, str):
            obj = misfit.lower()
            if obj in availableKeys:
                if obj == 'l2':
                    misfitFunction = mg.sl2
                elif obj == 'klx':
                    misfitFunction = mg.klx
            else:
                print('error: objective not available')
        else:
            misfitFunction = misfit
        model1=self.model.model1
        model0=self.model.model0
        geometry1 = self.model.geometry1
        geometry0 = self.model.geometry0

        self.bb = myDevito.myBB(model1,geometry1,model0, geometry0, ncores=ncores, misfitFunction=misfitFunction)


    def saveFig(self,  data, fileName):
        dict_data = self.model.dict_pars

        if data is None:
            nbl = self.model.model0.nbl
            slices = 2*[slice(nbl, -nbl)]
            data = {'C': self.model.model1.vp.data[slices].reshape(-1) , 'C0': self.model.model0.vp.data[slices].reshape(-1)}
        fig, axs = plt.subplots(1,len(data), figsize=(20,5))
        for i, (k, x) in enumerate(data.items()):
            # aux = axs[i].imshow(x.reshape(m.z['n'], -1, order='F'),interpolation='nearest')
            aux = axs[i].imshow(x.reshape(self.model.model0.shape[0], -1).T,interpolation='nearest')
            plt.colorbar(aux, ax=axs[i])
            axs[i].set_title(k)
            # nSources = len(shots) 
            # lengths = m.domain.get_lengths()
            # axs[i].plot([shots[0].receivers.receiver_list[r].position[0]*(m.x.n-1)/lengths[0] for r in range(m.x.n)],[shots[0].receivers.receiver_list[r].position[1]*(m.z.n-1)/lengths[1] for r in range(m.x.n)],'x',color= 'g',label='cobertura', markersize=2)

            # axs[i].plot([shots[s].sources.position[0]*(m.x.n-1)/lengths[0] for s in range(nSources)],[shots[s].sources.position[1]*(m.z.n-1)/lengths[1] for s in range(nSources)],'>',color= 'r',label='cobertura', markersize=2)

        fig.savefig(fileName)


    def bbFig(self, x=None, saveGrad=None, saveSeis=None): 

        nbl = self.model.model0.nbl
        slices = 2*[slice(nbl, -nbl)]

        if x is None:
            xx =  self.model.model0.vp.data[slices].reshape(-1,)
        elif isinstance(x, str):
            kk = x.lower()
            if kk == 'c0':
                xx =  self.model.model0.vp.data[slices].reshape(-1,)
            else:
                xx =  self.model.model1.vp.data[slices].reshape(-1,)
        else:
            xx = x

        f0, g0 = self.bb.fg(xx)

        if saveGrad is not None:
            self.saveFig({'v':xx, 'grad':g0}, saveGrad)

        return f0, g0

###
# class geopCase():
    # resultDir = None
    # freq = None
    # modelDict = None
    # def __init__(self, modelKey, objKey = 'OTx', freq=[14], files=None, source_depth=None): 
        # self.freq =  freq

        # home = os.getenv('HOME')
        # self.resultDir = os.path.join(home, 'geopetroResults', '-'.join([modelKey, objKey, str(freq), datetime.datetime.now().isoformat() ])) 
        # if not os.path.isdir(self.resultDir): 
            # os.makedirs(self.resultDir)

        # self.modelDict = geoPetro.gallery.read_data(fromGallery=modelKey)

        # if source_depth is not None:
            # self.modelDict['source_depth'] = source_depth
        # if files is not None:
            # codeDir = os.path.join(self.resultDir, 'code')
            # if not os.path.isdir(codeDir): 
                # os.makedirs(codeDir)
            # base = os.getcwd()
            # for ff in files:
                # shutil.copyfile(os.path.join(base,ff), os.path.join(codeDir, ff))
###


# def writeInfo(x,g,nIter,key, m, resultDir, shots=None, dict_data=None):
    # '''
    # m: model
    # '''
    # pyuoResDir = os.path.join(resultDir,key)
    # figDir = os.path.join(pyuoResDir, 'figures')
    # vDir = os.path.join(pyuoResDir, 'v')
    # gDir = os.path.join(pyuoResDir, 'g')
    # for dd in [figDir, vDir, gDir]:
        # if not os.path.isdir(dd): 
            # os.makedirs(dd)
    # # saving velocity models
    # x.tofile(os.path.join(vDir, '{}-v-{}.bin'.format(nIter, key)))
    # # saving gradient models
    # g.tofile(os.path.join(gDir, '{}-g-{}.bin'.format(nIter, key)))
    # # saving figures models
    # fig, axs = plt.subplots(1,2, figsize=(20,5))
    # if m.dim == 1:
        # axs[0].plot(x)
        # axs[0].set_title('v')
        # axs[1].plot(g)
        # axs[1].set_title('grad')
    # else:
        # aux = axs[0].imshow(x.reshape(m.z['n'], -1, order='F'),interpolation='nearest')
        # plt.colorbar(aux, ax=axs[0])
        # axs[0].set_title('v')
        # if shots is not None: 
            # nSources = len(shots) 
            # axs[0].plot([shots[0].receivers.receiver_list[r].position[0]*(m.x.n-1)/dict_data['base_physical_size'][0] for r in range(m.x.n)],[shots[0].receivers.receiver_list[r].position[1]*(m.z.n-1)/dict_data['base_physical_size'][1] for r in range(m.x.n)],'x',color= 'g',label='cobertura', markersize=2)

            # axs[0].plot([shots[s].sources.position[0]*(m.x.n-1)/dict_data['base_physical_size'][0] for s in range(nSources)],[shots[s].sources.position[1]*(m.z.n-1)/dict_data['base_physical_size'][1] for s in range(nSources)],'>',color= 'r',label='cobertura', markersize=2)

        # aux = axs[1].imshow(g.reshape(m.z['n'], -1, order='F'), interpolation='nearest')
        # plt.colorbar(aux, ax=axs[1])
        # axs[1].set_title('grad')

    # fig.savefig(os.path.join(figDir, '{}-{}.png'.format(nIter, key)))
    # plt.close(fig=fig)
    
# def pyuoCallBackF(state, m, resultDir, shots=None, dict_data=None):
    # x = state.xLast
    # g = state.gLast
    # nIter = state.nIter 
    # state.writeGLog(message=str(np.abs(x).max()), sharp=True, includeIter=True)

    # writeInfo(x,g,nIter, 'pyuo', m, resultDir, shots=shots, dict_data=dict_data)
