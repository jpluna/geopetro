import numpy as np

# import smooth as smooth
import geoPetro.smooth as smooth
# import ot

#### auxiliar functions
def vkl(a, b):
    '''
    computes the misfit of a into b
    '''
    zero = 1e-8
    pb = b>0
    pa = a>zero
    val = 0
    if any(pb):
        if any(pa):
            indAux = pa & pb
            val+= (a[indAux] * np.log(a[indAux]/b[indAux]) -a[indAux] + b[indAux]).sum()
        if any(~pa):
            val += b[ (~pa) & pb].sum()
    return val



###### misfit functions
def sl2(d_pred, d_obs, data=None):
    '''
    both matrices (2D np arrays) are the sismograms. Each colum is a trace.
    the function terutns the misfit and adjoint source
    '''
    residual =  d_pred - d_obs
    lres = residual.reshape(-1)
    fval = 0.5 * lres.dot(lres)
    return fval, residual


def klx(d_pred, d_obs, data=None):
    '''
    both matrices (2D np arrays) are the sismograms. Each colum is a trace.
    the function terutns the misfit and adjoint source
    '''
    tau = 1e-4
    # kernel = 'd'
    kernel = 'b'

    simSeis = d_pred.reshape(-1,)
    obsSeis = d_obs.reshape(-1,)

    xMass1, dxMass = smooth.smooth(simSeis,tau=tau, kernel=kernel)
    xMass2, _ = smooth.smooth(-obsSeis,tau=tau, kernel=kernel)
    xMass = xMass1 + xMass2
    del xMass1 
    del xMass2
    yMass1, dyMass = smooth.smooth(-simSeis,tau=tau,kernel=kernel)
    yMass2, _  = smooth.smooth(obsSeis,tau=tau, kernel=kernel)

    yMass = yMass1 + yMass2
    del yMass1 
    del yMass2
    xDivy = xMass/yMass

    val = vkl(xMass, yMass)
    indAux = (xMass>0) & (yMass>0)
    r = np.zeros_like(simSeis)
    r[indAux] = np.log(xDivy[indAux]) * dxMass[indAux] - (1 -  xDivy[indAux]) * dyMass[indAux]

    fval= val
    residual = r.reshape(d_obs.shape)

    return fval, residual

misfit_dict = {'l2': sl2,
               'kl': klx,
        }