# Std import block
###
import os
import numpy as np
import matplotlib.pyplot as plt
import time 
import shutil
import csv
from timeit import default_timer as timer
from pathlib import Path
from scipy.interpolate import interp1d
from obspy import read


import pysit
from pysit import *
from pysit.gallery import horizontal_reflector
from pysit.gallery import bp, marmousi

###


def getModel(model, freq=2.0, nSourses='max', tRange=None, source_depth=0,receiver_depth=0):
    __dataDir__ = os.path.join(os.path.dirname(__file__),  'modelData')

    
    if isinstance(nSourses,str):
        if nSourses!='max':
            nSourses='max'

    if model == '1d':

        # Setup
        #   Define Domain
        pmlz = PML(0.1, 100, ftype='quadratic')

        #   pmlz = Dirichlet()

        z_config = (0.1, 0.8, pmlz, Dirichlet())
        z_config = (0.1, 0.8, pmlz, pmlz)
        #   z_config = (0.1, 0.8, Dirichlet(), Dirichlet())

        d = RectangularDomain(z_config)

        # m = CartesianMesh(d, 301)
        m = CartesianMesh(d, 101)

        #   Generate true wave speed
        C, C0, m, d = horizontal_reflector(m)

        # Set up shots
        Nshots = 1
        shots = []

        # Define source location and type
        zpos = depth
        source = PointSource(m, (zpos), RickerWavelet(freq))

        # Define set of receivers
        receiver = PointReceiver(m, (zpos))
        # receivers = ReceiverSet([receiver])

        # Create and store the shot
        shot = Shot(source, receiver)
        # shot = Shot(source, receivers)
        shots.append(shot)


        # Define and configure the wave solver
        if tRange is None:
            trange = (0.0,1.0)
        else:
            trange = tRange

        solver2 = ConstantDensityAcousticWave(m,
                                             kernel_implementation='cpp',
                                             formulation='scalar',
                                             spatial_accuracy_order=2,
                                             trange=trange)
        #JP # Generate synthetic Seismic data
        #JP print('Generating data...')
        base_model = solver2.ModelParameters(m,{'C': C})
        #JP tt = time.time()
        #JP wavefields1 =  []
        #JP generate_seismic_data(shots, solver1, base_model, wavefields=wavefields1)
        #JP 
        #JP print('Data generation: {0}s'.format(time.time()-tt))
        #JP tt = time.time()
        wavefields2 =  []
        generate_seismic_data(shots, solver2, base_model, wavefields=wavefields2)
        #JP 
        #JP print('Data generation: {0}s'.format(time.time()-tt))
        #JP 
        # Define and configure the objective function
        resModel = {'m':m, 'C':C, 'C0':C0, 'solver':solver2, 'shots':shots} 


    elif model == '2d':
        # Setup

        #   Define Domain
        pmlx = PML(0.1, 100)
        pmlz = PML(0.1, 100)

        x_config = (0.1, 1.0, pmlx, pmlx)
        z_config = (0.1, 0.8, pmlz, pmlz)

        d = RectangularDomain(x_config, z_config)

        # m = CartesianMesh(d, 91, 71)
        m = CartesianMesh(d, 61, 41)

        #   Generate true wave speed
        C, C0, m, d = horizontal_reflector(m)


        # Set up shots
        #zmin = d.z.lbound
        #zmax = d.z.rbound
        #zpos = zmin + (1./9.)*zmax
        
        sources=nSourses
        shots = equispaced_acquisition(m,
                                       RickerWavelet(freq),
                                       sources=nSourses,
                                       source_depth=source_depth,
                                       source_kwargs={},
                                       receivers='max',
                                       receiver_depth=receiver_depth,
                                       receiver_kwargs={},
                                       )

        # Define and configure the wave solver
        if tRange is None:
            trange = (0.0,3.0)
        else:
            trange = tRange

        solver = ConstantDensityAcousticWave(m,
                                             spatial_accuracy_order=2,
                                             trange=trange,
                                             kernel_implementation='cpp')

        # Generate synthetic Seismic data
        tt = time.time()
        wavefields =  []
        base_model = solver.ModelParameters(m,{'C': C})
        generate_seismic_data(shots, solver, base_model, wavefields=wavefields)

        print('Data generation: {0}s'.format(time.time()-tt))

        # objective = TemporalLeastSquares(solver)

        resModel = {'m':m, 'C':C, 'C0':C0, 'solver':solver, 'shots':shots} 

    elif model == 'bola': 
        C, C0, m, d = marmousi(origin= np.array([0.0, 0.0]),pixel_scale='full', patch='mini-square',size = np.array([4800.0, 4800.0]),pixels = np.array([241, 241]) )   
        # v = np.fromfile('./modelData/bola/vel_circulo.bin', dtype=np.float32)
        v = np.fromfile( os.path.join(__dataDir__, 'bola','vel_circulo.bin'), dtype=np.float32)
        A= v.reshape(241,-1).T
        C=A.reshape(-1,1) 

        # v0 = np.fromfile('./modelData/bola/vp_homogeneous_expand.bin', dtype=np.float32)
        v0 = np.fromfile( os.path.join(__dataDir__, 'bola','vp_homogeneous_expand.bin'), dtype=np.float32)
        A0= v0.reshape(241,-1).T
        C0=A0.reshape(-1,1) 


        # Set up shots
        #zmin = d.z.lbound
        #zmax = d.z.rbound
        #zpos = zmin + (1./9.)*zmax
      

        shots = equispaced_acquisition(m, 
                RickerWavelet(freq), 
                sources=nSourses, 
                source_depth=source_depth, 
                source_kwargs={}, 
                receivers='max', 
                receiver_depth=receiver_depth, 
                receiver_kwargs={},)


        # Define and configure the wave solver
        if tRange is None:
            trange = (0.0,3.0)
        else:
            trange = tRange

        solver = ConstantDensityAcousticWave(m, 
                spatial_accuracy_order=2, 
                trange=trange, 
                kernel_implementation='cpp')

        # Generate synthetic Seismic data
        tt = time.time()
        wavefields =  []
        base_model = solver.ModelParameters(m,{'C': C})
        generate_seismic_data(shots, solver, base_model, wavefields=wavefields)


        print('Data generation: {0}s'.format(time.time()-tt))


        resModel = {'m':m, 'C':C, 'C0':C0, 'solver':solver, 'shots':shots} 
        
    elif model == 'bolaComBasePequena':
        C, C0, m, d = marmousi(origin= np.array([0.0, 0.0]),pixel_scale='full', patch='mini-square',size = np.array([4800.0, 4800.0]),pixels = np.array([241, 241]) )   
        # v = np.fromfile('./modelData/bolaComBasePequena/bolaComBasePequena.bin', dtype=np.float32)
        v = np.fromfile( os.path.join(__dataDir__, 'bolaComBasePequena','bolaComBasePequena.bin'), dtype=np.float32)
        A= v.reshape(241,-1).T
        C=A.reshape(-1,1) 

        # v0 = np.fromfile('./modelData/bola/vp_homogeneous_expand.bin', dtype=np.float32)
        v0 = np.fromfile( os.path.join(__dataDir__, 'bola','vp_homogeneous_expand.bin'), dtype=np.float32)
        A0= v0.reshape(241,-1).T
        C0=A0.reshape(-1,1) 


        # Set up shots
        #zmin = d.z.lbound
        #zmax = d.z.rbound
        #zpos = zmin + (1./4.)*zmax


        shots = equispaced_acquisition(m, 
                RickerWavelet(freq), 
                sources=nSourses, 
                source_depth=source_depth, 
                source_kwargs={}, 
                receivers='max', 
                receiver_depth=receiver_depth, 
                receiver_kwargs={},)


        # Define and configure the wave solver
        if tRange is None:
            trange = (0.0,3.0)
        else:
            trange = tRange

        solver = ConstantDensityAcousticWave(m, 
                spatial_accuracy_order=2, 
                trange=trange, 
                kernel_implementation='cpp')

        # Generate synthetic Seismic data
        tt = time.time()
        wavefields =  []
        base_model = solver.ModelParameters(m,{'C': C})
        generate_seismic_data(shots, solver, base_model, wavefields=wavefields)


        print('Data generation: {0}s'.format(time.time()-tt))
        resModel = {'m':m, 'C':C, 'C0':C0, 'solver':solver, 'shots':shots} 
        
    elif model == 'bolaSemBase': 
        C, C0, m, d = marmousi(origin= np.array([0.0, 0.0]),pixel_scale='full', patch='mini-square',size = np.array([4800.0, 4800.0]),pixels = np.array([241, 241]) )   
        v = np.fromfile('./modelData/bolaSemBase/bolaSemBase.bin', dtype=np.float32)
        A= v.reshape(241,-1).T
        C=A.reshape(-1,1) 

        v0 = np.fromfile('./modelData/bola/vp_homogeneous_expand.bin', dtype=np.float32)
        A0= v0.reshape(241,-1).T
        C0=A0.reshape(-1,1) 


        # Set up shots
        #zmin = d.z.lbound
        #zmax = d.z.rbound
        #zpos = zmin + (1./9.)*zmax
        #sources=30

        shots = equispaced_acquisition(m, 
                RickerWavelet(freq), 
                sources=nSourses, 
                source_depth=source_depth, 
                source_kwargs={}, 
                receivers='max', 
                receiver_depth=receiver_depth, 
                receiver_kwargs={},)


        # Define and configure the wave solver
        if tRange is None:
            trange = (0.0,3.0)
        else:
            trange = tRange

        solver = ConstantDensityAcousticWave(m, 
                spatial_accuracy_order=2, 
                trange=trange, 
                kernel_implementation='cpp')

        # Generate synthetic Seismic data
        tt = time.time()
        wavefields =  []
        base_model = solver.ModelParameters(m,{'C': C})
        generate_seismic_data(shots, solver, base_model, wavefields=wavefields)


        print('Data generation: {0}s'.format(time.time()-tt))


        resModel = {'m':m, 'C':C, 'C0':C0, 'solver':solver, 'shots':shots} 
        
    elif model == 'bolaGrande': 
        C, C0, m, d = marmousi(origin= np.array([0.0, 0.0]),pixel_scale='full', patch='mini-square',size = np.array([4800.0, 4800.0]),pixels = np.array([241, 241]) )   
        v = np.fromfile( os.path.join(__dataDir__, 'bolaGrande','bolaGrande.bin'), dtype=np.float32)
        A= v.reshape(241,-1).T
        C=A.reshape(-1,1) 

        v0 = np.fromfile(os.path.join(__dataDir__, 'bola','vp_homogeneous_expand.bin'), dtype=np.float32)
        A0= v0.reshape(241,-1).T
        C0=A0.reshape(-1,1) 


        # Set up shots
        #zmin = d.z.lbound
        #zmax = d.z.rbound
        #zpos = zmin + (1./9.)*zmax
        #sources=30

        shots = equispaced_acquisition(m, 
                RickerWavelet(freq), 
                sources=nSourses, 
                source_depth=source_depth, 
                source_kwargs={}, 
                receivers='max', 
                receiver_depth=receiver_depth, 
                receiver_kwargs={},)


        # Define and configure the wave solver
        if tRange is None:
            trange = (0.0,3.0)
        else:
            trange = tRange

        solver = ConstantDensityAcousticWave(m, 
                spatial_accuracy_order=2, 
                trange=trange, 
                kernel_implementation='cpp')

        # Generate synthetic Seismic data
        tt = time.time()
        wavefields =  []
        base_model = solver.ModelParameters(m,{'C': C})
        generate_seismic_data(shots, solver, base_model, wavefields=wavefields)


        print('Data generation: {0}s'.format(time.time()-tt))


        resModel = {'m':m, 'C':C, 'C0':C0, 'solver':solver, 'shots':shots} 
    
    elif model == 'marmousi2': 
        C, C0, m, d = marmousi(origin= np.array([0.0, 0.0]),pixel_scale='full', patch='mini-square',size = np.array([2304, 1212]),pixels = np.array([384 ,202]) )   
        v = np.fromfile( os.path.join(__dataDir__, 'marmousi2','marmousi2.bin'))
        #v = np.fromfile('./modelData/marmousi2/marmousi2.bin')
        A= v.reshape(384,-1)
        C=A.reshape(-1,1) 

        #v0 = np.fromfile('./modelData/marmousi2/vel_inicial_marmousi2.bin')
        v0 = np.fromfile( os.path.join(__dataDir__, 'marmousi2','vel_inicial_marmousi2.bin'))
        A0= v0.reshape(384,-1)
        C0=A0.reshape(-1,1) 


        # Set up shots
        #zmin = d.z.lbound
        #zmax = d.z.rbound
        #zpos = zmin + (1./8.)*zmax


        shots = equispaced_acquisition(m, 
                RickerWavelet(freq), 
                sources=nSourses, 
                source_depth=source_depth, 
                source_kwargs={}, 
                receivers='max', 
                receiver_depth=receiver_depth, 
                receiver_kwargs={},)


        # Define and configure the wave solver
        if tRange is None:
            trange = (0.0,3.0)
        else:
            trange = tRange

        solver = ConstantDensityAcousticWave(m, 
                spatial_accuracy_order=2, 
                trange=trange, 
                kernel_implementation='cpp')

        # Generate synthetic Seismic data
        tt = time.time()
        wavefields =  []
        base_model = solver.ModelParameters(m,{'C': C})
        generate_seismic_data(shots, solver, base_model, wavefields=wavefields)


        print('Data generation: {0}s'.format(time.time()-tt))


        resModel = {'m':m, 'C':C, 'C0':C0, 'solver':solver, 'shots':shots} 
        
    elif model == 'marmousiPequeno': 
        C, C0, m, d = marmousi(origin= np.array([0.0, 0.0]),pixel_scale='full', patch='mini-square',size = np.array([1152, 606]),pixels = np.array([192 ,101]) )   
        # v = np.fromfile('./modelData/marmousiPequeno/marmousiPequeno.bin')
        v = np.fromfile( os.path.join(__dataDir__, 'marmousiPequeno','marmousiPequeno.bin'))
        A= v.reshape(192,-1)
        C=A.reshape(-1,1) 

        # v0 = np.fromfile('./modelData/marmousiPequeno/vel_marmousiPequeno.bin')
        v0 = np.fromfile( os.path.join(__dataDir__, 'marmousiPequeno','vel_marmousiPequeno.bin'))
        A0= v0.reshape(192,-1)
        C0=A0.reshape(-1,1) 


        # Set up shots
        #zmin = d.z.lbound
        #zmax = d.z.rbound
        #zpos = zmin + (1./8.)*zmax


        shots = equispaced_acquisition(m, 
                RickerWavelet(freq), 
                sources=nSourses, 
                source_depth=source_depth, 
                source_kwargs={}, 
                receivers='max', 
                receiver_depth=receiver_depth, 
                receiver_kwargs={},)


        # Define and configure the wave solver
        if tRange is None:
            trange = (0.0,3.0)
        else:
            trange = tRange

        solver = ConstantDensityAcousticWave(m, 
                spatial_accuracy_order=2, 
                trange=trange, 
                kernel_implementation='cpp')

        # Generate synthetic Seismic data
        tt = time.time()
        wavefields =  []
        base_model = solver.ModelParameters(m,{'C': C})
        generate_seismic_data(shots, solver, base_model, wavefields=wavefields)


        print('Data generation: {0}s'.format(time.time()-tt))


        resModel = {'m':m, 'C':C, 'C0':C0, 'solver':solver, 'shots':shots}         
        
    return  resModel
###

def _load_models_only_horizontal_reflector(filename_case):
    dict_data = read_data(filename_case)
    mesh_x = dict_data['horizontal_reflector_mesh_x']
    mesh_z = dict_data['horizontal_reflector_mesh_z']
    trange = dict_data['trange']
    freq = dict_data['frequency']

    nSources = dict_data['nsources']
    if isinstance(nSources,str):
        if nSources!='max':
            nSources='max'
    source_depth = dict_data['source_depth']

    # Setup

    #   Define Domain
    pmlx = PML(0.1, 100)
    pmlz = PML(0.1, 100)

    x_config = (0.1, 1.0, pmlx, pmlx)
    z_config = (0.1, 0.8, pmlz, pmlz)

    d = RectangularDomain(x_config, z_config)

    m = CartesianMesh(d, mesh_x, mesh_z)

    #   Generate true wave speed
    C, C0, m, d = horizontal_reflector(m)
    res = {'C': C, 'C0': C0, 'd': d, 'm': m}
    return res

def load_models_only(filename_case):
    dict_data = read_data(fromGallery=filename_case)

    if 'is_horizontal_reflector' in dict_data and dict_data['is_horizontal_reflector'] == 1:
        return _load_models_only_horizontal_reflector(filename_case)

    water_depth = dict_data['water_depth']
    physical_origin = dict_data['physical_origin']
    base_physical_size = dict_data['base_physical_size']
    physical_dimensions_units = dict_data['physical_dimensions_units']
    base_pixels = dict_data['base_pixels']
    base_pixel_scale = dict_data['base_pixel_scale']
    base_pixel_units = dict_data['base_pixel_units']
    factor = dict_data['factor']

    file_vel = dict_data['file_vel']
    file_vel0 = dict_data['file_vel0']
    transpose_data = bool(dict_data['transpose_data'])
    if dict_data['type_data'] == 'float32':
        type_data = np.float32
    elif dict_data['type_data'] == 'float':
        type_data = float
    else:
        raise ValueError('Invalid file type')

    trange = dict_data['trange']

    nSources = dict_data['nsources']
    if isinstance(nSources,str):
        if nSources!='max':
            nSources='max'
    source_depth = dict_data['source_depth']

    coordinate_lbounds = physical_origin
    coordinate_rbounds = physical_origin + base_physical_size

    pixel_scale = 1/factor * base_pixel_scale
    length = coordinate_rbounds - coordinate_lbounds
    pixels = np.ceil(length/pixel_scale).astype(int)
    # Update physical size
    physical_size = pixel_scale * pixels
    pixels += 1 # include last point in all dimensions

    # extract or default the boundary conditions
    x_lbc = PML(0.1*physical_size[0], 100.0)
    x_rbc = PML(0.1*physical_size[0], 100.0)
    z_lbc = PML(0.1*physical_size[1], 100.0)
    z_rbc = PML(0.1*physical_size[1], 100.0)

    x_config = (coordinate_lbounds[0], coordinate_rbounds[0], x_lbc, x_rbc)
    z_config = (coordinate_lbounds[1], coordinate_rbounds[1], z_lbc, z_rbc)
    d = RectangularDomain(x_config, z_config)

    m = CartesianMesh(d, *pixels)

    config = {  'base_physical_origin': physical_origin, 
                'base_physical_size': base_physical_size,
                'base_pixel_scale': base_pixel_scale,
                'base_pixels': base_pixels,
                'pixel_scale': pixel_scale,
                'pixels': pixels
                }

    print('Loading models...')
    tstart = timer()
    vp = _read_model(file_vel, type_data=type_data)
    if transpose_data:
        vp = vp.T
    if vp.shape[0] == vp.size:
        # In this case, data was stored as a column vector
        # So we need to reshape it
        vp = vp.reshape(base_pixels)


    # vp = resample_data(vp, config)
    vp = _resample_data(vp, config)

    vp0 = _read_model(file_vel0, type_data=type_data)
    if transpose_data:
        vp0 = vp0.T
    if vp0.shape[0] == vp0.size:
        # In this case, data was stored as a column vector
        # So we need to reshape it
        vp0 = vp0.reshape(base_pixels)
    vp0 = _resample_data(vp0, config)

    grid = m.mesh_coords()
    ZZ = grid[-1].reshape(vp.shape)
    loc = np.where(ZZ < water_depth)
    vp0[loc] = vp[loc]

    C = vp.reshape(m.shape())
    C0 = vp0.reshape(m.shape())
    tend = timer()
    print(f'Loading models... {tend-tstart}s')

    res = {'C': C, 'C0': C0, 'd': d, 'm': m}
    return res

def _get_model_horizontal_reflector(dict_data, plot=False):
    mesh_x = dict_data['horizontal_reflector_mesh_x']
    mesh_z = dict_data['horizontal_reflector_mesh_z']
    trange = dict_data['trange']
    freq = dict_data['frequency']

    nSources = dict_data['nsources']
    if isinstance(nSources,str):
        if nSources!='max':
            nSources='max'
    source_depth = dict_data['source_depth']

    # Setup

    #   Define Domain
    pmlx = PML(0.1, 100)
    pmlz = PML(0.1, 100)

    x_config = (0.1, 1.0, pmlx, pmlx)
    z_config = (0.1, 0.8, pmlz, pmlz)

    d = RectangularDomain(x_config, z_config)

    m = CartesianMesh(d, mesh_x, mesh_z)

    #   Generate true wave speed
    C, C0, m, d = horizontal_reflector(m)
    
    sources=nSources
    shots = equispaced_acquisition(m,
                                    RickerWavelet(freq),
                                    sources=nSources,
                                    source_depth=source_depth,
                                    source_kwargs={},
                                    receivers='max',
                                    receiver_kwargs={},
                                    )

    solver = ConstantDensityAcousticWave(m,
                                            spatial_accuracy_order=2,
                                            trange=trange,
                                            kernel_implementation='cpp')

    # Generate synthetic Seismic data
    tt = time.time()
    wavefields =  []
    base_model = solver.ModelParameters(m,{'C': C})
    generate_seismic_data(shots, solver, base_model, wavefields=wavefields)

    print('Data generation: {0}s'.format(time.time()-tt))

    resModel = {'m':m, 'C':C, 'C0':C0, 'solver':solver, 'shots':shots} 
    return resModel

def get_model_from_dict(dict_data, plot=False):
    if 'is_horizontal_reflector' in dict_data and dict_data['is_horizontal_reflector'] == 1:
        return _get_model_horizontal_reflector(dict_data, plot)
    
    freq = dict_data['frequency']
    water_depth = dict_data['water_depth']
    physical_origin = dict_data['physical_origin']
    base_physical_size = dict_data['base_physical_size']
    physical_dimensions_units = dict_data['physical_dimensions_units']
    base_pixels = dict_data['base_pixels']
    base_pixel_scale = dict_data['base_pixel_scale']
    base_pixel_units = dict_data['base_pixel_units']
    factor = dict_data['factor']
    type_data = dict_data['type_data']

    file_vel = dict_data['file_vel']
    file_vel0 = dict_data['file_vel0']
    transpose_data = bool(dict_data['transpose_data'])
    if dict_data['type_data'] == 'float32':
        type_data = np.float32
    elif dict_data['type_data'] == 'float':
        type_data = float
    else:
        raise ValueError('Invalid file type')
    trange = dict_data['trange']

    nSources = dict_data['nsources']
    if isinstance(nSources,str):
        if nSources!='max':
            nSources='max'
    source_depth = dict_data['source_depth']

    if 'receiver_depth' in dict_data: 
        receiver_depth = dict_data['receiver_depth']
        print('setting receiver_depth from dictionary')
    else: 
        receiver_depth= source_depth 
        print('setting receiver_depth source_depth  ')


    coordinate_lbounds = physical_origin
    coordinate_rbounds = physical_origin + base_physical_size

    pixel_scale = 1/factor * base_pixel_scale
    length = coordinate_rbounds - coordinate_lbounds
    pixels = np.ceil(length/pixel_scale).astype(int)
    # Update physical size
    physical_size = pixel_scale * pixels
    pixels += 1 # include last point in all dimensions

    # extract or default the boundary conditions
    x_lbc = PML(0.1*physical_size[0], 100.0)
    x_rbc = PML(0.1*physical_size[0], 100.0)
    z_lbc = PML(0.1*physical_size[1], 100.0)
    z_rbc = PML(0.1*physical_size[1], 100.0)

    x_config = (coordinate_lbounds[0], coordinate_rbounds[0], x_lbc, x_rbc)
    z_config = (coordinate_lbounds[1], coordinate_rbounds[1], z_lbc, z_rbc)
    d = RectangularDomain(x_config, z_config)

    m = CartesianMesh(d, *pixels)

    vp = _read_model(file_vel, type_data)

    if vp.shape[0] == vp.size:
        # In this case, data was stored as a column vector
        # So we need to reshape it
        vp = vp.reshape(base_pixels)
    if transpose_data:
        vp = vp.T
    config = {  'base_physical_origin': physical_origin, 
                'base_physical_size': base_physical_size,
                'base_pixel_scale': base_pixel_scale,
                'base_pixels': base_pixels,
                'pixel_scale': pixel_scale,
                'pixels': pixels
                }
    vp = _resample_data(vp, config)

    grid = m.mesh_coords()
    ZZ = grid[-1].reshape(vp.shape)

    vp0 = _read_model(file_vel0, type_data)
    if vp0.shape[0] == vp0.size:
        # In this case, data was stored as a column vector
        # So we need to reshape it
        vp0 = vp0.reshape(base_pixels)
    if transpose_data:
        vp0 = vp0.T
    vp0 = _resample_data(vp0, config)

    loc = np.where(ZZ < water_depth)
    vp0[loc] = vp[loc]

    C = vp.reshape(m.shape())
    C0 = vp0.reshape(m.shape())

    if plot:
        fig = plt.figure(figsize=(8,6))
        vis.plot(C, m)
        plt.title('Model')

        fig = plt.figure(figsize=(8,6))
        vis.plot(C0, m)
        plt.title('Initial velocity')

    # Set up shots
    tstart = timer()
    print('Setting up shots...')
    shots = equispaced_acquisition(m,
                                   RickerWavelet(freq),
                                   sources=nSources,
                                   source_depth=source_depth,
                                   source_kwargs={},
                                   receivers='max', 
                                   receiver_depth=receiver_depth,
                                   receiver_kwargs={},
                                   )
    tend = timer()
    print(f'Setting up shots... {tend-tstart}')

    # Define and configure the wave solver
    # trange = (0.0, 3.0)
    tstart = timer()
    print('Configuring the wave solver...')
    solver = ConstantDensityAcousticWave(m,
                                         spatial_accuracy_order=6,
                                         trange=trange,
                                         kernel_implementation='cpp')
    tend = timer()
    print(f'Configuring the wave solver... {tend-tstart}')


    # Generate synthetic Seismic data
    print('Data generation...')
    tstart = timer()
    base_model = solver.ModelParameters(m,{'C': C})
    wavefields = []
    generate_seismic_data(shots, solver, base_model, wavefields=wavefields, save_method='pickle', verbose=True)

    resModel = {'m':m, 'C':C, 'C0':C0, 'solver':solver, 'shots':shots}
    return resModel

def _resample_data(arr, param):
    """ Resamples the specified parameter to the sizes, from the original."""

    # Resample the array, in each dimension, with nearest neighbor interpolation.
    y = arr
    for i in range(arr.ndim):
        x = param['base_physical_origin'][i] + np.arange(param['base_pixels'][i])*param['base_pixel_scale'][i]
        I = interp1d(x, y, copy=False, axis=i, kind='linear')
        # I = interp1d(x, y, copy=False, axis=i, kind='nearest')

        new_x = param['base_physical_origin'][i] + np.arange(param['pixels'][i])*param['pixel_scale'][i]

        # Bounds that go slightly past the true thing should extend from the right side
        new_x[new_x > param['base_physical_size'][i]] = param['base_physical_size'][i]

        y = I(new_x)

    return y

def _read_model(filename, type_data=np.float32):
    '''
    Read a model file
    Currently there are two options:
    - a segy file (if the extension is .sgy or .segy), which uses obspy to read the file
    - a binary file (if the extension is different), which uses numpy
    '''
    extension = Path(filename).suffix
    if extension == '.sgy' or extension == '.segy':
        stream = read(filename)
        data = np.array([t.data for t in stream.traces])
    elif extension == '.csv':
        data = np.loadtxt(filename, delimiter=',', dtype=type_data)
    else:
        # if needed, we can create other cases here, such as text files, .mat, etc
        # for now, let's assume it's a binary file, so let's use numpy
        # some files have a float32 type. I'll assume this is the norm
        data = np.fromfile(filename, dtype=type_data)
    return data


# def read_data_new(case_name, from_gallery=False, custom_pars=None):
#     fixPath = False
#     if from_gallery is True: 
#         __rootDir__ = os.path.dirname(__file__)
#         filename_case = os.path.join(__rootDir__, 'modelData_csv', f'data_{case_name}.csv')
#         fixPath=True
#     else:
#         filename_case = case_name

#     dict_data = {}
#     with open(filename) as csv_file:
#         csv_reader = csv.reader(csv_file, delimiter=',')
#         line_count = 0
#         for row in csv_reader:
#             key = row[0]
#             if len(row) == 2:
#                 # Line contains only two columns, so it can be a string or a float
#                 if row[1][0] != "'" and row[1][0] != '"':
#                     # float
#                     val = float(row[1])
#                 else: 
#                     # string
#                     val = row[1]
#                     val = val.replace('"', '')
#                     val = val.replace("'", '')
#             else:
#                 # Line contains more than two coluns, so it is a float array
#                 ncols = len(row)-1
#                 val = np.zeros(ncols)
#                 for i,c in enumerate(row[1:]):
#                     c = c.replace('[', '')
#                     c = c.replace(']', '')
#                     val[i] = float(c)
#             dict_data[key] = val
    
#     # Fill default values if they were not in the file
#     _default_data(dict_data)

#     if 'is_horizontal_reflector' in dict_data and dict_data['is_horizontal_reflector'] == 1:
#         # In this case there is no file_vel or file_vel0, so we return the dict_data without the following code
#         return dict_data
    
#     ### JP
#     breakpoint()
#     if fixPath:
#         # dict_data['file_vel'] = os.path.join(__rootDir__, dict_data['file_vel'] )
#         # dict_data['file_vel0'] = os.path.join(__rootDir__, dict_data['file_vel0'] )
#         dict_data['file_vel'] = os.path.join(__rootDir__, 'modelData_csv', fromGallery, dict_data['file_vel'])
#         dict_data['file_vel0'] = os.path.join(__rootDir__, 'modelData_csv', fromGallery, dict_data['file_vel0'])

#     ### end JP
#     return dict_data

def read_data(filename=None, fromGallery=None):
    '''
    Read case data from a csv
    '''
    ### JP
    fixPath = False
    if filename is None: 
        __rootDir__ = os.path.dirname(__file__)
        filename = os.path.join(__rootDir__, 'modelData_csv', fromGallery)
        fixPath=True
    ### end JP

    dict_data = {}
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            key = row[0]
            if len(row) == 2:
                # Line contains only two columns, so it can be a string or a float
                if row[1][0] != "'" and row[1][0] != '"':
                    # float
                    val = float(row[1])
                else: 
                    # string
                    val = row[1]
                    val = val.replace('"', '')
                    val = val.replace("'", '')
            else:
                # Line contains more than two coluns, so it is a float array
                ncols = len(row)-1
                val = np.zeros(ncols)
                for i,c in enumerate(row[1:]):
                    c = c.replace('[', '')
                    c = c.replace(']', '')
                    val[i] = float(c)
            dict_data[key] = val
    
    # Fill default values if they were not in the file
    _default_data(dict_data)

    if 'is_horizontal_reflector' in dict_data and dict_data['is_horizontal_reflector'] == 1:
        # In this case there is no file_vel or file_vel0, so we return the dict_data without the following code
        return dict_data
    
    ### JP
    breakpoint()
    if fixPath:
        # dict_data['file_vel'] = os.path.join(__rootDir__, dict_data['file_vel'] )
        # dict_data['file_vel0'] = os.path.join(__rootDir__, dict_data['file_vel0'] )
        dict_data['file_vel'] = os.path.join(__rootDir__, 'modelData_csv', fromGallery, dict_data['file_vel'])
        dict_data['file_vel0'] = os.path.join(__rootDir__, 'modelData_csv', fromGallery, dict_data['file_vel0'])

    ### end JP
    return dict_data


def _default_data(dict_data):
    '''
    Data that is usually default and might not need to be changed
    '''

    if 'is_horizontal_reflector' in dict_data and dict_data['is_horizontal_reflector'] == 1:
        return dict_data

    if 'physical_origin' not in dict_data:
        dict_data['physical_origin'] =  np.array([0.0, 0.0])
    if 'physical_dimensions_units' not in dict_data:
        dict_data['physical_dimensions_units'] = ('m', 'm')
    if 'base_pixel_units' not in dict_data:
        dict_data['base_pixel_units'] = ('m', 'm')
    if 'factor' not in dict_data:
        dict_data['factor'] = 1
    if 'transpose_data' not in dict_data:
        dict_data['transpose_data'] = False
    if 'frequency' not in dict_data:
        dict_data['frequency'] = 10.0
    if 'type_data' not in dict_data:
        dict_data['type_data'] = 'float32'
    dict_data['base_pixels'] = dict_data['base_pixels'].astype(int)

def getModelNew(filename_case):
    '''
    Wrapper to read case data and get the model
    '''
    dict_data = read_data(filename_case)
    res = get_model_from_dict(dict_data)
    return res


if __name__ == '__main__':
    # # (1) define the data dictionary manually
    # dict_data = {}
    # dict_data['water_depth'] = 32.0
    # dict_data['physical_origin'] =  np.array([0.0, 0.0])
    # dict_data['base_physical_size'] = np.array([9200.0, 3000.0])
    # dict_data['trange'] = (0.0, 3.0)
    # dict_data['nSources'] = 1
    # dict_data['source_depth'] = 500
    # dict_data['base_pixels'] = np.array([2301, 751])
    # dict_data['base_pixel_scale'] = np.array([4.0, 4.0])
    # dict_data['factor'] = 0.1
    # dict_data['file_vel'] = 'modelData/marmousi/velocity_rev1.segy'
    # dict_data['file_vel0'] = 'modelData/marmousi/velocity_blur.bin'
    # res = get_model_from_dict(dict_data)

    # # (2) load the case from a csv file
    # dict_data = read_data('modelData/data_marmousi.csv')
    # res = get_model_from_dict(dict_data)

    # # (3) get model directly from file
    # res = getModelNew('modelData/data_marmousi.csv')
    res = getModelNew('modelData/data_horizontal_reflector.csv')

    # For debugging purposes, let's plot the results
    plt.ion()

    plt.figure()
    vis.plot(res['C'], res['m'])
    plt.title('Model')

    plt.figure()
    vis.plot(res['C0'], res['m'])
    plt.title('Initial model')

    # (4) load from file and generate model for different resampling factors
    # resampling_factors = [0.1, 0.25, 0.33]
    # plt.ion()
    # resampling_factors = [0.1, 0.25, 0.33, 0.5, 0.75, 1]
    # dict_data = read_data('modelData/data_bola.csv')
    # res = {}
    # for r in resampling_factors:
    #     dict_data['factor'] = r
    #     print(f'Getting model using a resampling factor of {r}')
    #     res[r] = get_model_from_dict(dict_data, plot=True)
    #     print()


