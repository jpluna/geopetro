import os
import math
from mpi4py import MPI
import numpy as np

from multiprocessing import Pool, cpu_count

from timeit import default_timer as timer

from pysit.objective_functions.objective_function import ObjectiveFunctionBase
from pysit.util.parallel import ParallelWrapShotNull
from pysit.modeling.temporal_modeling import TemporalModeling

class myObj(ObjectiveFunctionBase):
    """ How to compute the parts of the objective you need to do optimization """

    def __init__(self, solver, parallel_wrap_shot=ParallelWrapShotNull(), imaging_period = 1, l2w=0.5, ncores=None):
        """imaging_period: Imaging happens every 'imaging_period' timesteps. Use higher numbers to reduce memory consumption at the cost of lower gradient accuracy.
            By assigning this value to the class, it will automatically be used when the gradient function of the temporal objective function is called in an inversion context.
        """
        self.solver = solver
        self.modeling_tools = TemporalModeling(solver)

        self.parallel_wrap_shot = parallel_wrap_shot

        self.imaging_period = int(imaging_period) #Needs to be an integer
        # self.l2w=l2w

        # Set number of processes to be used by the multiprocessing Pool
        if ncores == None:
            self.ncores = cpu_count()
        else:
            self.ncores = ncores

        # nts = len(self.solver.ts())
        # coords = np.arange(2 * nts).reshape(-1, 1) 
        # self.otM = ot.dist(coords,coords,'sqeuclidean')


    def _residual(self, shot, m0, dWaveOp=None, wavefield=None):
        """Computes residual in the usual sense.

        Parameters
        ----------
        shot : pysit.Shot
            Shot for which to compute the residual.
        dWaveOp : list of ndarray (optional)
            An empty list for returning the derivative term required for
            computing the imaging condition.

        """

        # If we will use the second derivative info later (and this is usually
        # the case in inversion), tell the solver to store that information, in
        # addition to the solution as it would be observed by the receivers in
        # this shot (aka, the simdata).
        rp = ['simdata']
        if dWaveOp is not None:
            rp.append('dWaveOp')
        # If we are dealing with variable density, we want the wavefield returned as well.
        if wavefield is not None:
           rp.append('wavefield')

        # Run the forward modeling step
        retval = self.modeling_tools.forward_model(shot, m0, self.imaging_period, return_parameters=rp)

        # Compute the residual vector by interpolating the measured data to the
        # timesteps used in the previous forward modeling stage.
        # resid = map(lambda x,y: x.interpolate_data(self.solver.ts())-y, shot.gather(), retval['simdata'])
        simSeismo = retval['simdata']
        obsSeismo = shot.receivers.interpolate_data(self.solver.ts()) 
        # resid = shot.receivers.interpolate_data(self.solver.ts()) - retval['simdata']

        # If the second derivative info is needed, copy it out
        if dWaveOp is not None:
            dWaveOp[:]  = retval['dWaveOp'][:]
        if wavefield is not None:
            wavefield[:] = retval['wavefield'][:]

        return simSeismo, obsSeismo
    
    def _solve_shot_pool(self, dict_shot):
        ind = dict_shot['ind']
        shot = dict_shot['shot']
        m0 = dict_shot['m0']
        print(f'Evaluating shot {ind}...')

        objVal = 0
        dt = self.solver.dt

        tstart = timer()
        sim, obs = self._residual(shot, m0) 
        aux = self.myMisfit(sim, obs, dt)
        tend = timer()
        print(f'Evaluating shot {ind}... ok. fval {aux:.5g}, {tend-tstart:.3g}s')
        return aux

    def evaluate(self, shots, m0):
        arr_shots = []
        for ind, shot in enumerate(shots):
            arr_shots.append({  'ind': ind,
                                'shot': shot,
                                'm0': m0,
            })
        
        tstart = timer()
        with Pool(processes=self.ncores) as pool:
            # issue tasks asynchronously
            async_result = pool.map_async(self._solve_shot_pool, arr_shots)
            # wait for the task to complete and get the iterable of return values
            # print('Waiting for results...')
            objval_shots = async_result.get()

        objval = sum([o for o in objval_shots])
        tend = timer()
        print(f'objval = {objval:.5g}')
        print(f'Evaluate all shots: {tend-tstart:.3g}')

        return objval

    def _gradient_helper(self, shot, m0, ignore_minus=False, ret_pseudo_hess_diag_comp = False, **kwargs):
        """Helper function for computing the component of the gradient due to a
        single shot.

        Computes F*_s(d - scriptF_s[u]), in our notation.

        Parameters
        ----------
        shot : pysit.Shot
            Shot for which to compute the residual.

        """

        # Compute the residual vector and its norm
        dWaveOp=[]

        # If this is true, then we are dealing with variable density. In this case, we want our forward solve
        # To also return the wavefield, because we need to take gradients of the wavefield in the adjoint model
        # Step to calculate the gradient of our objective in terms of m2 (ie. 1/rho)
        if hasattr(m0, 'kappa') and hasattr(m0,'rho'):
            wavefield=[]
        else:
            wavefield=None
            
        # r = self._residual(shot, m0, dWaveOp=dWaveOp, wavefield=wavefield, **kwargs)
        simSeis, obsSeis = self._residual(shot, m0, dWaveOp=dWaveOp, wavefield=wavefield, **kwargs)
        
        # Perform the migration or F* operation to get the gradient component
        # g = self.modeling_tools.migrate_shot(shot, m0, r, self.imaging_period, dWaveOp=dWaveOp, wavefield=wavefield)
        dt = self.solver.dt
        auxVal, r = self.myAdjSource(simSeis, obsSeis, dt)
        g = self.modeling_tools.migrate_shot(shot, m0, r, self.imaging_period, dWaveOp=dWaveOp, wavefield=wavefield)

        if not ignore_minus:
            g = -1*g

        if ret_pseudo_hess_diag_comp:
            return g, r, self._pseudo_hessian_diagonal_component_shot(dWaveOp)
        else:
            return auxVal, g, r

    def _compute_gradient_shot(self, dict_shot):
        ind = dict_shot['ind']
        shot = dict_shot['shot']
        m0 = dict_shot['m0']
        aux_info = dict_shot['aux_info']

        print(f'Computing gradient for shot {ind}...')
        tstart = timer()
        if ('pseudo_hess_diag' in aux_info) and aux_info['pseudo_hess_diag'][0]:
            g, r, h = self._gradient_helper(shot, m0, ignore_minus=True, ret_pseudo_hess_diag_comp = True)
            pseudo_h_diag += h 
        else:
            auxVal, g, r = self._gradient_helper(shot, m0, ignore_minus=True)
        
        # grad -= g # handle the minus 1 in the definition of the gradient of this objective
        g *= -1
        tend = timer()
        print(f'Computing gradient for shot {ind}... ok. {tend-tstart:.3g}s')

        dict_shot['aux'] = auxVal
        dict_shot['g'] = g
        return dict_shot
        
    def compute_gradient(self, shots, m0, aux_info={}, **kwargs):
        arr_shots = []
        for ind, shot in enumerate(shots):
            arr_shots.append({'ind': ind, 
                              'shot': shot,
                              'm0': m0,
                              'aux_info': aux_info,
            })

        # compute the portion of the gradient due to each shot
        tstart = timer()
        grad = m0.perturbation()
        fval = 0.0
        pseudo_h_diag = np.zeros(m0.asarray().shape)

        if self.ncores == 1:
            res_shots = [self._compute_gradient_shot(arr_shots[0])]
        else:
            with Pool(processes=self.ncores) as pool:
                # issue tasks asynchronously
                async_result = pool.map_async(self._compute_gradient_shot, arr_shots)
                # wait for the task to complete and get the iterable of return values
                res_shots = async_result.get()
        
        grad_shots = sum([res['g'].data for res in res_shots])
        grad += grad_shots
        fval = sum([res['aux'] for res in res_shots])
        tend = timer()
        print(f'Compute gradient for all shots: {tend-tstart:.3g}')

        if ('objective_value' in aux_info) and aux_info['objective_value'][0]:
            aux_info['objective_value'] = (True, fval)

        # # store any auxiliary info that is requested
        # # if ('residual_norm' in aux_info) and aux_info['residual_norm'][0]:
        #     # aux_info['residual_norm'] = (True, np.sqrt(r_norm2))
        # if ('objective_value' in aux_info) and aux_info['objective_value'][0]:
        #     aux_info['objective_value'] = (True, fval)
        # # if ('pseudo_hess_diag' in aux_info) and aux_info['pseudo_hess_diag'][0]:
        #     # aux_info['pseudo_hess_diag'] = (True, pseudo_h_diag)

        return grad

    def myMisfit(self, simSeis, obsSeis, dt): 
        '''
        the result should be scaled with dt
        '''
        raise NotImplementedError("Must be implemented by subclass.")

    def myAdjSource(self, simSeis, obsSeis, dt): 
        '''
        the result should NOT be scaled with dt
        return: val, adjSource
        '''
        raise NotImplementedError("Must be implemented by subclass.")

