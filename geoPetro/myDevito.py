### 
import numpy as np
from multiprocessing import Pool, cpu_count, current_process
import matplotlib.pyplot as plt
from inspect import signature
from devito import Function
from examples.seismic import AcquisitionGeometry, Receiver
from examples.seismic.acoustic import AcousticWaveSolver
from mpl_toolkits.axes_grid1 import make_axes_locatable

# import misfitGallery
# import geoPetro.misfitGallery

from devito import configuration
configuration['log-level'] = 'WARNING'
### 
###

def plot_image(data, vmin=None, vmax=None, colorbar=True, cmap="gray", title=None, figsize=(8,5)):
    """
    Plot image data, such as RTM images or FWI gradients.

    Parameters
    ----------
    data : ndarray
        Image data to plot.
    cmap : str
        Choice of colormap. Defaults to gray scale for images as a
        seismic convention.
    """
    fig = plt.figure(figsize=figsize)
    if vmin is None:
        vmin = np.min(data)
    if vmax is None:
        vmax = np.max(data)
    plot = plt.imshow(1e3*np.transpose(data),
                    vmin=vmin*1e3,
                    vmax=vmax*1e3,
                    cmap=cmap,
                    # aspect='auto',
                    interpolation='nearest')
    plot.axes.xaxis.set_visible(False)
    plot.axes.yaxis.set_visible(False)
    if title is not None:
        plt.title(title, fontdict={'fontsize': 14})

    # Create aligned colorbar on the right
    if colorbar:
        ax = plt.gca()
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        plt.colorbar(plot, cax=cax)
        cax.yaxis.set_tick_params(labelsize=10)
    fig.tight_layout()
    plt.show()
    return fig, plot, cax

# Serial modeling function
def forward_modeling_single_shot(model, geometry, save=False, dt=4):
    solver = AcousticWaveSolver(model, geometry, space_order=4)
    d_obs, u0 = solver.forward(vp=model.vp, save=save)[0:2]
    return d_obs.resample(dt), u0

def forward_modeling_single_shot_wrapper(dict_pars):
    ind = dict_pars['ind']
    model = dict_pars['model']
    geometry = dict_pars['geometry']
    # print(f'Calling forward_modeling_single_shot, ind {ind+1}, by {current_process().name}')
    d_obs, u0 = forward_modeling_single_shot(model, geometry)
    return {'ind': ind, 'd_obs': d_obs, 'u0': u0}

# Serial modeling function
def forward_modeling_multi_shots(model, geometry, save=False, dt=4):
    shots = []
    print('Calling forward_modeling_multi_shots')
    for i in range(geometry.nsrc):
        # print(f'shot {i+1}')
        # Geometry for current shot
        geometry_i = AcquisitionGeometry(model, geometry.rec_positions, geometry.src_positions[i,:], 
                                         geometry.t0, geometry.tn, f0=geometry.f0, src_type=geometry.src_type)
        # Call serial modeling function for each index
        d_obs_i, _ = forward_modeling_single_shot(model, geometry_i, save=save, dt=dt)
        shots.append(d_obs_i)
    return shots

# Parallel modeling function
def forward_modeling_multi_shots_parallel(model, geometry, save=False, dt=4, ncores='max'):
    dict_pars = []
    for i in range(geometry.nsrc):
        # Geometry for current shot
        geometry_i = AcquisitionGeometry(model, geometry.rec_positions, geometry.src_positions[i,:], 
                                         geometry.t0, geometry.tn, f0=geometry.f0, src_type=geometry.src_type)
        # Call serial modeling function for each index
        dict_pars_curr = {'ind': i,
                          'model': model,
                          'geometry': geometry_i}
        dict_pars.append(dict_pars_curr)

    if ncores == 'max':
        ncores = cpu_count()
    print(f'Calling forward_modeling_single_shot_pool in parallel using {ncores} cores')
    with Pool(processes=ncores) as pool:
        async_result = pool.map_async(forward_modeling_single_shot_wrapper, dict_pars)
        dict_output = async_result.get()

    dict_shots = {}   
    for d in dict_output:
        dict_shots[d['ind']] = d['d_obs']
    # Not sure if the order is relevant. Just in case, let's add shots in the correct order
    shots = []
    for i in range(geometry.nsrc):
        shots.append(dict_shots[i])
    return shots

class myBB():
    model=None
    geometry=None
    d_obs=None
    model0=None
    geometry0=None
    misfitFunction=None
    useParallel=None

    def __init__(self, model, geometry, model0, geometry0, misfitFunction=None, ncores='max'):
        self.model=model
        self.model0 = model0
        self.geometry=geometry
        self.geometry0 = geometry0

        if ncores == 'max':
            self.ncores = cpu_count()
        else:
            self.ncores = ncores
        
        if self.ncores == 1:
            self.useParallel = False
        else:
            self.useParallel = True

        if self.useParallel:
            self.d_obs = forward_modeling_multi_shots_parallel(model, geometry, save=False, ncores=ncores)
        else:
            self.d_obs = forward_modeling_multi_shots(model, geometry, save=False)
        print('data generated')
        self.misfitFunction = misfitFunction

    def _fwi_objective_single_shot(self, single_geometry, single_d_obs):
        model = self.model0
        geometry = single_geometry
        d_obs = single_d_obs

        grad = Function(name='grad', grid=model.grid)
        residual = Receiver(name='rec', grid=model.grid,
                            time_range=geometry.time_axis,
                            coordinates=geometry.rec_positions)
        solver = AcousticWaveSolver(model, geometry, space_order=4)

        d_pred, u0 = solver.forward(vp=model.vp, save=True)[0:2]

        sig = signature(self.misfitFunction)
        if len(sig.parameters) == 2:
            fval, residualData = self.misfitFunction(d_pred.data[:], 
                                                     d_obs.resample(geometry.dt).data[:][0:d_pred.data.shape[0],:])
        elif len(sig.parameters) == 3:
            data = {'model': model,
                    'geometry': geometry}
            fval, residualData = self.misfitFunction(d_pred.data[:], 
                                                     d_obs.resample(geometry.dt).data[:][0:d_pred.data.shape[0],:],
                                                     data=data)
        else:
            raise ValueError('Misfit function contains wrong number of arguments')

        # Sometimes variable fval above comes back from the misfit function with type Data, 
        # which is a Devito type. 
        # This is because d_pred and d_obs are Data.
        # Devito wants a float fval. So we force this by putting fval in an array container.
        # Otherwise Devito will throw a warning at every call
        fval = np.array(fval)
        residual.data[:] = residualData

        solver.gradient(rec=residual, u=u0, vp=model.vp, grad=grad)

        grad_crop = np.array(grad.data[:])[model.nbl:-model.nbl, model.nbl:-model.nbl]
        return fval, grad_crop

    def _fwi_objective_single_shot_wrapper(self, dict_pars):
        # This wrapper is used for the parallel call
        ind = dict_pars['ind']
        geometry = dict_pars['geometry']
        d_obs = dict_pars['d_obs']
        # print(f'Calling fwi_objective_single_shot, ind {ind+1}, by {current_process().name}')
        fval, grad_crop = self._fwi_objective_single_shot(geometry, d_obs)
        return {'ind': ind, 'fval': fval, 'grad': grad_crop}

    def fwi_objective_multi_shots(self):
        # Serial
        model = self.model0
        geometry = self.geometry0
        d_obs = self.d_obs
        fval_shots = []
        grad_shots = []
        print('Calling fwi_objective_multi_shots')
        for i in range(geometry.nsrc):
            # print(f'shot {i+1}')
            geometry_i = AcquisitionGeometry(model, geometry.rec_positions, geometry.src_positions[i,:], 
                                             geometry.t0, geometry.tn, f0=geometry.f0, src_type=geometry.src_type)
            fval_i, grad_i = self._fwi_objective_single_shot(geometry_i, d_obs[i])
            fval_shots.append(fval_i)
            grad_shots.append(grad_i)

        fval = 0
        grad = np.zeros(model.shape)
        for i in range(geometry.nsrc):
            fval += fval_shots[i]
            grad += grad_shots[i]
        return fval, grad

    def fwi_objective_multi_shots_parallel(self):
        model = self.model0
        geometry = self.geometry0
        d_obs = self.d_obs
        dict_pars = []
        for i in range(geometry.nsrc):
            geometry_i = AcquisitionGeometry(model, geometry.rec_positions, geometry.src_positions[i,:], 
                                            geometry.t0, geometry.tn, f0=geometry.f0, src_type=geometry.src_type)
            dict_pars_curr = {'ind': i,
                            'model': model,
                            'geometry': geometry_i,
                            'd_obs': d_obs[i]}
            dict_pars.append(dict_pars_curr)

        print(f'Calling fwi_objective_single_shot in parallel using {self.ncores} cores')
        with Pool(processes=self.ncores) as pool:
            # issue tasks asynchronously
            async_result = pool.map_async(self._fwi_objective_single_shot_wrapper, dict_pars)
            # wait for the task to complete and get the iterable of return values
            dict_output = async_result.get()

        fval = 0
        grad = np.zeros(model.shape)
        for d in dict_output:
            fval += d['fval']
            grad += d['grad']
        return fval, grad

    def fg(self, x):
        model = self.model0

        v_curr = x.reshape(model.shape)
        model.update('vp', v_curr.reshape(model.shape))



        print(f'vmin = {v_curr.min()}, vmax = {v_curr.max()}')



        self.model0 = model
        if self.useParallel:
            fval, grad = self.fwi_objective_multi_shots_parallel()
        else:
            fval, grad = self.fwi_objective_multi_shots()
        grad = grad.flatten().astype(np.float64)

        print(f'myBB.fg, fval = {fval}, |grad| = {np.linalg.norm(grad)}')
        # breakpoint()

        return fval, -grad

    def fg_slowness(self, x):
        model = self.model0

        v_curr = 1.0/np.sqrt(x.reshape(model.shape))
        model.update('vp', v_curr.reshape(model.shape))



        print(f'xmin = {x.min()}, vmax = {x.max()}')
        print(f'vmin = {v_curr.min()}, vmax = {v_curr.max()}')



        self.model0 = model
        if self.useParallel:
            fval, grad = self.fwi_objective_multi_shots_parallel()
        else:
            fval, grad = self.fwi_objective_multi_shots()
        grad = grad.flatten().astype(np.float64)

        print(f'myBB.fg_slowness, fval = {fval}, |grad| = {np.linalg.norm(grad)}')

        return fval, grad