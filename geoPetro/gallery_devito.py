import numpy as np
from pathlib import Path
from timeit import default_timer as timer
from examples.seismic.model import SeismicModel
from examples.seismic import AcquisitionGeometry

import geoPetro.gallery_base as glrb
# import gallery_base as glrb

def read_params_file(case_name, from_gallery=True, custom_pars=None):
    return glrb.read_params_file(case_name, from_gallery, custom_pars)

def get_models_devito(case_name, from_gallery=True, custom_pars=None):
    dict_data = glrb.read_params_file(case_name, from_gallery, custom_pars)
    glrb.print_data(dict_data)
        
    # water_depth = dict_data['water_depth']
    physical_origin = dict_data['physical_origin']
    base_physical_size = dict_data['base_physical_size']
    physical_dimensions_units = dict_data['physical_dimensions_units']
    base_pixels = dict_data['base_pixels']
    base_pixel_scale = dict_data['base_pixel_scale']
    base_pixel_units = dict_data['base_pixel_units']
    factor = dict_data['factor']

    transpose_data = bool(dict_data['transpose_data'])
    if dict_data['type_data'] == 'float32':
        type_data = np.float32
    elif dict_data['type_data'] == 'float':
        type_data = float
    else:
        raise ValueError('Invalid file type')

    coordinate_lbounds = physical_origin
    coordinate_rbounds = physical_origin + base_physical_size

    pixel_scale = 1/factor * base_pixel_scale
    length = coordinate_rbounds - coordinate_lbounds
    pixels = np.ceil(length/pixel_scale).astype(int)
    # Update physical size
    # physical_size = pixel_scale * pixels
    pixels += 1 # include last point in all dimensions

    config = {  'base_physical_origin': physical_origin, 
                'base_physical_size': base_physical_size,
                'base_pixel_scale': base_pixel_scale,
                'base_pixels': base_pixels,
                'pixel_scale': pixel_scale,
                'pixels': pixels
                }

    print('Loading models...')
    tstart = timer()
    vp = glrb.read_model(dict_data['file_vel'], type_data=type_data)
    if vp.shape[0] == vp.size:
        # In this case, data was stored as a column vector
        # So we need to reshape it
        vp = vp.reshape(pixels)
    if transpose_data:
        vp = vp.T

    vp0 = glrb.read_model(dict_data['file_vel0'], type_data=type_data)
    if vp0.shape[0] == vp0.size:
        # In this case, data was stored as a column vector
        # So we need to reshape it
        vp0 = vp0.reshape(pixels)
    if transpose_data:
        vp0 = vp0.T

    # Resampling
    vp = glrb.resample_data(vp, config)
    vp0 = glrb.resample_data(vp0, config)

    tend = timer()
    print(f'Loading models... {tend-tstart:.2f}s')

    # We need velocities in km/s
    vp /= 1e3
    vp0 /= 1e3

    print('Creating devito models...')
    tstart = timer()

    nbl = dict_data['nbl']
    space_order = dict_data['space_order']
    origin = tuple(physical_origin)
    spacing = dict_data['base_pixel_scale']/dict_data['factor']
    model1 = SeismicModel(space_order=space_order, vp=vp, origin=origin, shape=vp.shape,
                                dtype=np.float32, spacing=spacing, nbl=nbl, bcs="damp",)
    model0 = SeismicModel(space_order=space_order, vp=vp0, origin=origin, shape=vp0.shape,
                                dtype=np.float32, spacing=spacing, nbl=nbl, bcs="damp",
                                grid=model1.grid)
    tend = timer()
    print(f'Creating devito models... {tend-tstart:.2f}s')

    return model1, model0, dict_data

def get_geometry_devito(dict_data, model1, model0):
    # Sources, receivers and geometry
    print('Creating sources, receivers and geometry...')
    tstart = timer()
    nsources = int(dict_data['nsources'])
    source_depth = dict_data['source_depth']
    src_coordinates = np.empty((nsources, 2))
    src_coordinates[:, 1] = source_depth
    src_step = model1.domain_size[0]/(nsources+1)
    src_coordinates[:, 0] = src_step*np.arange(1, nsources+1)

    nreceivers = int(dict_data['nreceivers'])
    rec_coordinates = np.empty((nreceivers, 2))
    rec_coordinates[:, 0] = np.linspace(0, model1.domain_size[0], num=nreceivers)
    rec_coordinates[:, 1] = source_depth

    trange = dict_data['trange']
    t0 = trange[0] * 1e3
    tn = trange[1] * 1e3

    f0 = dict_data['frequency']/1e3
    geometry1 = AcquisitionGeometry(model1, rec_coordinates, src_coordinates, t0, tn, f0=f0, src_type='Ricker')
    geometry0 = AcquisitionGeometry(model0, rec_coordinates, src_coordinates, t0, tn, f0=f0, src_type='Ricker')
    tend = timer()
    print(f'Creating sources, receivers and geometry... {tend-tstart:.2f}s')

    return geometry1, geometry0